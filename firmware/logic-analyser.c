/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include <dma.h>
#include <logic-analyser.h>
#include <led-controls.h>
#include <usb_protocol.h>

/* Prescaler and count values associated with the support sample rates */
#define PRESCALE_200KHZ		0
#define COUNT_200KHZ		240

#define PRESCALE_100KHZ		0
#define COUNT_100KHZ		480

#define PRESCALE_50KHZ		0
#define COUNT_50KHZ			960

#define PRESCALE_20KHZ		0
#define COUNT_20KHZ			2400

#define PRESCALE_10KHZ		0
#define COUNT_10KHZ			4800

#define PRESCALE_5KHZ		0
#define COUNT_5KHZ			9600

#define PRESCALE_2KHZ		0
#define COUNT_2KHZ			24000

#define PRESCALE_1KHZ		0
#define COUNT_1KHZ			48000

#define PRESCALE_500HZ		15
#define COUNT_500HZ			6000

#define PRESCALE_200HZ		15
#define COUNT_200HZ			15000

#define PRESCALE_100HZ		15
#define COUNT_100HZ			30000

#define PRESCALE_50HZ		127
#define COUNT_50HZ			7500

#define PRESCALE_20HZ		127
#define COUNT_20HZ			18750

#define PRESCALE_10HZ		127
#define COUNT_10HZ			37500

#define PRESCALE_5HZ		1023
#define COUNT_5HZ			9375

#define PRESCALE_2HZ		1023
#define COUNT_2HZ			23438		//Note: This is rounded up

#define PRESCALE_1HZ		1023
#define COUNT_1HZ			46875

/* 12 bit analogue sampling reduces the max pin count at a given frequency */
#define SAMPLE_200KHZ_A_PIN_COUNT	 2
#define SAMPLE_100KHZ_A_PIN_COUNT	 4
#define SAMPLE_50KHZ_A_PIN_COUNT	 8

logic_analyser_config_t logic_config = {
	.sample_rate_set = false,
	.capture_quantity = 0, /* continuous sampling */
	.porta_enabled = 0, /* no pins enabled */
	.portb_enabled = 0,
};

void reset_logic_state(void)
{
	logic_config.sample_rate_set = false;
	logic_config.capture_quantity = 0; /* continuous sampling */
	logic_config.porta_enabled = 0;
	logic_config.portb_enabled = 0;
}

uint8_t set_sample_rate(char *usb_data)
{
	uint32_t prescaler;
	uint32_t count;

	switch(usb_data[PACKET_DATA_START]){
		case(SAMPLE_200KHZ):
			prescaler = PRESCALE_200KHZ;
			count = COUNT_200KHZ;
			logic_config.sample_rate = SAMPLE_200KHZ;
			break;
		case(SAMPLE_100KHZ):
			prescaler = PRESCALE_100KHZ;
			count = COUNT_100KHZ;
			logic_config.sample_rate = SAMPLE_100KHZ;
			break;
		case(SAMPLE_50KHZ):
			prescaler = PRESCALE_50KHZ;
			count = COUNT_50KHZ;
			logic_config.sample_rate = SAMPLE_50KHZ;
			break;
		case(SAMPLE_20KHZ):
			prescaler = PRESCALE_20KHZ;
			count = COUNT_20KHZ;
			logic_config.sample_rate = SAMPLE_20KHZ;
			break;
		case(SAMPLE_10KHZ):
			prescaler = PRESCALE_10KHZ;
			count = COUNT_10KHZ;
			logic_config.sample_rate = SAMPLE_10KHZ;
			break;
		case(SAMPLE_5KHZ):
			prescaler = PRESCALE_5KHZ;
			count = COUNT_5KHZ;
			logic_config.sample_rate = SAMPLE_5KHZ;
			break;
		case(SAMPLE_2KHZ):
			prescaler = PRESCALE_2KHZ;
			count = COUNT_2KHZ;
			logic_config.sample_rate = SAMPLE_2KHZ;
			break;
		case(SAMPLE_1KHZ):
			prescaler = PRESCALE_1KHZ;
			count = COUNT_1KHZ;
			logic_config.sample_rate = SAMPLE_1KHZ;
			break;
		case(SAMPLE_500HZ):
			prescaler = PRESCALE_500HZ;
			count = COUNT_500HZ;
			logic_config.sample_rate = SAMPLE_500HZ;
			break;
		case(SAMPLE_200HZ):
			prescaler = PRESCALE_200HZ;
			count = COUNT_200HZ;
			logic_config.sample_rate = SAMPLE_200HZ;
			break;
		case(SAMPLE_100HZ):
			prescaler = PRESCALE_100HZ;
			count = COUNT_100HZ;
			logic_config.sample_rate = SAMPLE_100HZ;
			break;
		case(SAMPLE_50HZ):
			prescaler = PRESCALE_50HZ;
			count = COUNT_50HZ;
			logic_config.sample_rate = SAMPLE_50HZ;
			break;
		case(SAMPLE_20HZ):
			prescaler = PRESCALE_20HZ;
			count = COUNT_20HZ;
			logic_config.sample_rate = SAMPLE_20HZ;
			break;
		case(SAMPLE_10HZ):
			prescaler = PRESCALE_10HZ;
			count = COUNT_10HZ;
			logic_config.sample_rate = SAMPLE_10HZ;
			break;
		case(SAMPLE_5HZ):
			prescaler = PRESCALE_5HZ;
			count = COUNT_5HZ;
			logic_config.sample_rate = SAMPLE_5HZ;
			break;
		case(SAMPLE_2HZ):
			prescaler = PRESCALE_2HZ;
			count = COUNT_2HZ;
			logic_config.sample_rate = SAMPLE_2HZ;
			break;
		case(SAMPLE_1HZ):
			prescaler = PRESCALE_1HZ;
			count = COUNT_1HZ;
			logic_config.sample_rate = SAMPLE_1HZ;
			break;
		default:
			return USB_SETTING_ERROR;
	}

	timer_set_prescaler(TIM1, prescaler);
	timer_set_period(TIM1, count);

	logic_config.sample_rate_set = true;

	return USB_SETTING_SUCCESS;
}

uint8_t set_capture_length(char *usb_data)
{
	uint64_t capture_length = 0;
	uint8_t no_sign;
	unsigned i;

	/* assemble 64-bit capture length from big-endian command */
	for (i = PACKET_DATA_START; i <= PACKET_DATA_START + 7; i++) {
		no_sign = usb_data[i];
		capture_length |= ((uint64_t) no_sign) << (8 * ((PACKET_DATA_START + 7) - i));
	}

	if (capture_length < MIN_SAMPLE_LEN || capture_length > MAX_SAMPLE_LEN) {
		/* 0 means continuous sampling */
		if (capture_length != 0) {
			return USB_SETTING_ERROR;
		}
	}

	/* Setting was successful */
	logic_config.capture_quantity = capture_length;

	return USB_SETTING_SUCCESS;
}

void select_channels(char *usb_data)
{
	logic_config.porta_enabled = usb_data[PACKET_DATA_START];
	logic_config.portb_enabled = usb_data[PACKET_DATA_START + 1];
}

uint8_t start_acquisition(void)
{
	/* Check required parameters have been set */
	if (!logic_config.sample_rate_set)
	{
		return USB_SETTING_ERROR;
	}
	if (!logic_config.porta_enabled && !logic_config.portb_enabled)
	{
		return USB_SETTING_ERROR;
	}

	uint16_t portA = logic_config.porta_enabled;
	uint16_t portB = logic_config.portb_enabled << 8;
	uint16_t pins_configured = portB | portA;
	uint8_t num_pins;

	/* Calculate the number of enabled pins */
	/* Certain speeds are only supported with less than 8 pins enabled */
	/* If analogue functionality is enabled then, the number is even lower */
	for (num_pins = 0; pins_configured; num_pins++) {
		pins_configured &= pins_configured - 1;
	}

	if (logic_config.porta_enabled) {
		dma_start_sampling_porta();
	}
	if (logic_config.portb_enabled) {
		dma_start_sampling_portb();
	}

	/* Indicate to user acquiring */
	turn_led_blue();

	/* Start timer and return success */
	timer_enable_counter(TIM1);
	return USB_SETTING_SUCCESS;
}

void stop_acquisition(void)
{
	if (logic_config.porta_enabled) {
		dma_stop_sampling_porta();
	}
	if (logic_config.portb_enabled) {
		dma_stop_sampling_portb();
	}

	timer_disable_counter(TIM1);

	/* Indicate to user acquisition stopped */
	turn_led_off();
}
