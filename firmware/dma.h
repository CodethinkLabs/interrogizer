/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTEROGIZER_DMA_H
#define INTEROGIZER_DMA_H

void dma_init(void);

void dma_start_sampling_porta(void);
void dma_start_sampling_portb(void);

void dma_stop_sampling_porta(void);
void dma_stop_sampling_portb(void);

/*
 * Get a new buffer with samples for portb. This will only return each buffer
 * once. After that returning NULL until the next buffer is ready. Buffers are
 * MAX_PACKET_LEN bytes long.
 */
uint8_t *dma_get_new_portb_buffer(void);
uint8_t *dma_get_new_porta_buffer(void);

void dma_release_porta_buffer(void);
void dma_release_portb_buffer(void);

#endif /* INTEROGIZER_DMA_H */
