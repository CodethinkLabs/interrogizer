/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/f0/nvic.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>

/* Make two references to three global variables used in the initialisation.c */
/* These are needed to avoid too much processing in the USB RX callback       */
extern char usb_host_data[64];
extern uint32_t usb_host_data_len;
extern uint32_t usb_host_status;


extern const struct usb_device_descriptor dev;
extern const struct usb_endpoint_descriptor comm_endp[];
extern const struct usb_endpoint_descriptor data_endp[];

typedef struct
__attribute__((packed))
{
    struct usb_cdc_header_descriptor header;
    struct usb_cdc_call_management_descriptor call_mgmt;
    struct usb_cdc_acm_descriptor acm;
    struct usb_cdc_union_descriptor cdc_union;
} usb_functional_descriptors_t;

extern const usb_functional_descriptors_t cdcacm_functional_descriptors;

extern const struct usb_interface_descriptor comm_iface[];
extern const struct usb_interface_descriptor data_iface[];
extern const struct usb_interface ifaces[];
extern const struct usb_config_descriptor config;

extern const char *usb_strings[];

/* Function declarations */

enum usbd_request_return_codes cdcacm_control_request(usbd_device *usbd_dev,
		struct usb_setup_data *req, uint8_t **buf, uint16_t *len, 
		void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req));

void usb_setup(void);

void timer1_init(void);

void led_setup(void);

void gpio_function_generator_setup_a(void);

void gpio_function_generator_setup_b(void);

void gpio_channels_setup_a(void);

void gpio_channels_setup_b(void);

void gpio_debug_setup(void);
