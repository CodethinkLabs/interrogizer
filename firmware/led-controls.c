/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <led-controls.h>

#include <libopencm3/stm32/gpio.h>

/* LED Pins (All on cathode) per HW Rev A:
 * PB0 = Red
 * PB1 = Green
 * PB3 = Blue */

void turn_led_red(void)
{
	/* Turn the red LED on */
	gpio_clear(GPIOB, GPIO0);

	/* Ensure other colour LEDs are off */
	gpio_set(GPIOB, GPIO1 | GPIO3);
	return;
}

void turn_led_green(void)
{
	/* Turn the green LED on */
	gpio_clear(GPIOB, GPIO1);

	/* Ensure other colour LEDs are off */
	gpio_set(GPIOB, GPIO0 | GPIO3);
	return;
}

void turn_led_blue(void)
{
	/* Turn the blue LED on */
	gpio_clear(GPIOB, GPIO3);

	/* Ensure other colour LEDs are off */
	gpio_set(GPIOB, GPIO0 | GPIO1);
	return;
}

void turn_led_off(void)
{
	/* Turn the LED off */
	gpio_set(GPIOB, GPIO0 | GPIO1 | GPIO3);

	return;
}
