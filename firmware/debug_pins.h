/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PINS_H
#define PINS_H

/* GPIOs we can mess with to indicate program state
 * CH* reffers to oscilloscope channels.
 * e.g. we might use gpio_toggle(DEBUG_PINS_PORT, DEBUG_CH1) in an interrupt
 * handler and use an oscilloscope or logic analyser to measure the frequency
 * of this toggling
 */
#define DEBUG_PINS_PORT GPIOB
#define DEBUG_CH1	GPIO4
#define DEBUG_CH2	GPIO5
#define DEBUG_CH3	GPIO6
#define DEBUG_CH4	GPIO7

#endif
