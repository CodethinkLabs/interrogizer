/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stddef.h>

#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/f0/nvic.h>
#include <libopencm3/stm32/f0/syscfg.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

#include "debug_pins.h"
#include "dma.h"
#include "usb_protocol.h"
#include "led-controls.h"

static void dma_porta_isr(void);
static void dma_portb_isr(void);

enum usb_out_buffer_state {
	NONE,		/* No part of the buffer has valid samples */
	LOWER_HALF,	/* The lower half of the buffer has valid samples */
	UPPER_HALF,	/* The upper half of the buffer has valid samples */
};

/*
 * DMA continually fills and refills the buffer with GPIO samples.
 * We get interrupts when DMA is half way and when DMA resets to the start.
 * Using these interrupts we can always be sending (over USB) the half that
 * DMA isn't writing to.
 */
struct port_dma_state {
	/* 2 buffers: at any one time samples are read into one, while USB
	   is sending the other */
	uint8_t buf[NUM_SAMPLES * 2];
	/* Identifies the half-buffer to send over USB */
	volatile enum usb_out_buffer_state output_state;
#ifdef DEBUG_DMA_BUFFERS
	volatile enum usb_out_buffer_state soft_state;
#endif

	/* read-only parameters */

	/* identify which dma channel to use */
	const uint32_t dma_controller;
	const uint8_t dma_channel;
	const uint8_t irqn;
	const enum rcc_periph_clken dma_clk;

	/* identify which timer to use */
	const uint32_t timer;
	/* TIM_DIER_CCxDE (enable dma request on output compare x) */
	const uint32_t timer_cc;

	/* peripheral address (the IDR register for the right GPIO port) */
	const uintptr_t periph_addr;

	/* debug gpio pin from debug_pins.h to hold high while handling interrupt */
	const uint8_t debug_gpio;

};

static struct port_dma_state porta = {
	.output_state = NONE,
#ifdef DEBUG_DMA_BUFFERS
	.soft_state = NONE,
#endif
	.dma_controller = DMA1,
	.dma_channel = DMA_CHANNEL2,
	.dma_clk = RCC_DMA1,
	.irqn = NVIC_DMA1_CHANNEL2_3_DMA2_CHANNEL1_2_IRQ,
	.timer = TIM1,
	.timer_cc = TIM_DIER_CC1DE,
	.periph_addr = (uintptr_t) &GPIO_IDR(GPIOA),
	.debug_gpio = DEBUG_CH3,
};

static struct port_dma_state portb = {
	.output_state = NONE,
	.dma_controller = DMA1,
	.dma_channel = DMA_CHANNEL3,
	.dma_clk = RCC_DMA2,
	.irqn = NVIC_DMA1_CHANNEL2_3_DMA2_CHANNEL1_2_IRQ,
	.timer = TIM1,
	.timer_cc = TIM_DIER_CC2DE,
	/* +1 because we want pins 8-15 */
	.periph_addr = ((uintptr_t) &GPIO_IDR(GPIOB)) + 1,
	.debug_gpio = DEBUG_CH4,
};

void dma1_channel2_3_dma2_channel1_2_isr(void)
{
	dma_porta_isr();
	dma_portb_isr();
}

static void __dma_isr(struct port_dma_state *port)
{
	const uint32_t controller = port->dma_controller;
	const uint8_t channel = port->dma_channel;
	const uint8_t gpio = port->debug_gpio;

	/* if this interrupt is for this port */
	if (DMA_ISR(controller) & (DMA_ISR_TCIF(channel) | DMA_ISR_HTIF(channel))) {
		gpio_set(DEBUG_PINS_PORT, gpio);

		/* reset interrupt flags
		 * this assumes we aren't delayed so badly that a half and full
		 * buffer interrupt are queued together. This is reasonable
		 * because we only get an interrupt every 64 samples and this
		 * isr is fast (around 1us)
		 */
		dma_clear_interrupt_flags(controller, channel, DMA_TCIF | DMA_HTIF);

		/*
		 * We get our first interrupt when the DMA has filled the lower half
		 * of the buffer, after this it is safe for us to hand that lower half
		 * over to USB.
		 * We then get an interrupt for each half of the buffer that DMA fills.
		 * Keep tracking the output state one half behind the DMA.
		 */
		switch (port->output_state) {
		case NONE:
			port->output_state = LOWER_HALF;
			break;
		case LOWER_HALF:
			port->output_state = UPPER_HALF;
			break;
		case UPPER_HALF:
			port->output_state = LOWER_HALF;
			break;
		}

#ifdef DEBUG_DMA_BUFFERS
		if (port->output_state == port->soft_state) {
			/* DMA overtook software */
			while (1) {
				// white-ish ~1/3 brightness
				turn_led_red();
				turn_led_blue();
				turn_led_green();
			}
		}
#endif

		gpio_clear(DEBUG_PINS_PORT, gpio);
	}
}

static void dma_porta_isr(void)
{
	__dma_isr(&porta);
}

static void dma_portb_isr(void)
{
	__dma_isr(&portb);
}

static uint8_t *__dma_get_new_buffer(struct port_dma_state *port,
			enum usb_out_buffer_state *last_state)
{
	uint8_t *ret = NULL;

	/* assuming we can read this atomically */
	if (port->output_state != *last_state) {
		switch (port->output_state) {
		case NONE:
			break;
		case LOWER_HALF:
			ret = &port->buf[0];
			break;
		case UPPER_HALF:
			ret = &port->buf[NUM_SAMPLES];
			break;
		}

		*last_state = port->output_state;
#ifdef DEBUG_DMA_BUFFERS
		port->soft_state = port->output_state;
#endif
	}

	return ret;
}

uint8_t *dma_get_new_porta_buffer(void)
{
	static enum usb_out_buffer_state last_state = NONE;
	return __dma_get_new_buffer(&porta, &last_state);
}

uint8_t *dma_get_new_portb_buffer(void)
{
	static enum usb_out_buffer_state last_state = NONE;
	return __dma_get_new_buffer(&portb, &last_state);
}

#ifdef DEBUG_DMA_BUFFERS
static void __dma_release_soft_buffer(struct port_dma_state *port)
{
	port->soft_state = NONE;
}

void dma_release_porta_buffer(void)
{
	__dma_release_soft_buffer(&porta);
}

void dma_release_portb_buffer(void)
{
	__dma_release_soft_buffer(&portb);
}
#else
void dma_release_porta_buffer(void) {}
void dma_release_portb_buffer(void) {}
#endif

static void __dma_start_sampling(struct port_dma_state *port)
{
	port->output_state = NONE;
	nvic_enable_irq(port->irqn);
	dma_enable_channel(port->dma_controller, port->dma_channel);
}

void dma_start_sampling_porta(void)
{
	__dma_start_sampling(&porta);
}

void dma_start_sampling_portb(void)
{
	__dma_start_sampling(&portb);
}

static void __dma_stop_sampling(struct port_dma_state *port)
{
	dma_disable_channel(port->dma_controller, port->dma_channel);
	nvic_disable_irq(port->irqn);
}

void dma_stop_sampling_porta(void)
{
	__dma_stop_sampling(&porta);
}

void dma_stop_sampling_portb(void)
{
	__dma_stop_sampling(&portb);
}

static void __dma_init(struct port_dma_state *port)
{
	const uint32_t controller = port->dma_controller;
	const uint8_t channel = port->dma_channel;
	const enum rcc_periph_clken clk = port->dma_clk;
	const uint8_t gpio = port->debug_gpio;
	const uint32_t timer = port->timer;
	const uint32_t timer_cc = port->timer_cc;
	const uintptr_t periph_addr = port->periph_addr;

	gpio_clear(DEBUG_PINS_PORT, gpio);

	rcc_periph_clock_enable(clk);

	dma_channel_reset(controller, channel);

	dma_set_peripheral_address(controller, channel, (uint32_t) periph_addr);

	dma_set_memory_address(controller, channel, (uint32_t) port->buf);
	dma_set_number_of_data(controller, channel, NUM_SAMPLES * 2);

	dma_set_peripheral_size(controller, channel, DMA_CCR_PSIZE_8BIT);
	dma_set_memory_size(controller, channel, DMA_CCR_MSIZE_8BIT);

	dma_set_read_from_peripheral(controller, channel);

	dma_enable_memory_increment_mode(controller, channel);
	dma_enable_transfer_complete_interrupt(controller, channel);
	dma_enable_half_transfer_interrupt(controller, channel);
	dma_enable_circular_mode(controller, channel);

	TIM_DIER(timer) |= timer_cc;
}

void dma_init(void)
{
	__dma_init(&porta);
	__dma_init(&portb);
}
