/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/st_usbfs.h>
#include <libopencm3/usb/usbd.h>

#include "counter.h"
#include "debug_pins.h"
#include "initialisation.h"
#include "usb.h"
#include "usb_protocol.h"

/* See st_usbfs_copy_to_pm in libopencm3/lib/stm32/st_usbfs_v2.c */
static void __usb_copy_to_pm(volatile void *vPM, uint8_t cmd, const void *samples)
{
	const uint8_t *lbuf = samples;
	volatile uint16_t *PM = vPM;
	uint32_t i;

	*PM++ = ((uint16_t)get_send_counter()) << 8 | cmd;

	for (i = 0; i < NUM_SAMPLES - 1; i+= 2) {
		*PM++ = (uint16_t)lbuf[i+1] << 8 | lbuf[i];
	}
}

/* st_usbfs_double_ep_write_packet modified for cmd byte */
static uint16_t __usb_write_samples_double(uint8_t addr, uint8_t cmd, const void *samples)
{
	addr &= 0x7F;

	if (USB_GET_EP_SW_BUF_TX(addr)) {
		/* buffer 1 */
		__usb_copy_to_pm(USB_GET_EP_TX_BUFF_1(addr), cmd, samples);
		USB_SET_EP_TX_COUNT_1(addr, MAX_PACKET_LEN);

		USB_TGL_EP_TX_SW_BUF(addr);

		/*
		 * Block until hardware is done sending the previous buffer.
		 * If we don't wait then this function could be called again
		 * and will st_usbfs_copy_to_pm into that buffer before it is
		 * all sent.
		 */
		while(!USB_GET_EP_HW_BUF_TX(addr));
	} else {
		/* buffer 0 */
		__usb_copy_to_pm(USB_GET_EP_TX_BUFF_0(addr), cmd, samples);
		USB_SET_EP_TX_COUNT_0(addr, MAX_PACKET_LEN);

		USB_TGL_EP_TX_SW_BUF(addr);

		/* block until hardware is done sending the previous buffer */
		while(USB_GET_EP_HW_BUF_TX(addr));
	}

	/* assume the first transaction isn't sending samples */

	return MAX_PACKET_LEN;
}

void usb_write_samples_double(uint8_t addr, uint8_t cmd, const void *samples)
{
	gpio_set(DEBUG_PINS_PORT, DEBUG_CH1);

	__usb_write_samples_double(addr, cmd, samples);

	gpio_clear(DEBUG_PINS_PORT, DEBUG_CH1);
}

void cdcacm_data_rx_cb(usbd_device *usbd_dev, uint8_t ep)
{
	(void)ep;

	usb_host_data_len = usbd_ep_read_packet(usbd_dev, 0x01, usb_host_data, 64);
	usb_host_status = 1;
}

void cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
	(void)wValue;

	usbd_ep_setup(usbd_dev, 0x01, USB_ENDPOINT_ATTR_BULK, 64, cdcacm_data_rx_cb);
	usbd_ep_setup(usbd_dev, 0x82, USB_ENDPOINT_ATTR_BULK, 64, NULL);
	usbd_ep_setup(usbd_dev, 0x83, USB_ENDPOINT_ATTR_INTERRUPT, 16, NULL);

	usbd_register_control_callback(
				usbd_dev,
				USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
				USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
				cdcacm_control_request);
}
