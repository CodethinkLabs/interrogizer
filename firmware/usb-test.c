/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <libopencm3/stm32/f0/nvic.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/usbd.h>

#include "debug_pins.h"

#include "counter.h"
#include "dma.h"
#include "initialisation.h"
#include "logic-analyser.h"
#include "led-controls.h"
#include "usb.h"
#include "usb_protocol.h"
#include "function-generator.h"

#define GPIO_READ_PINS GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7
#define ECHO_MAX_DATA (MAX_PACKET_LEN - PACKET_DATA_START - 1)

/* Buffer to be used for control requests. */
static uint8_t usbd_control_buffer[128];

static usbd_device *usbd_dev;

/* Logic analyser global variables */
static bool data_acquire = false;

static void usb_setting_return(uint8_t usb_return)
{
	uint16_t n_written;

	do {
		n_written = usbd_ep_write_packet(usbd_dev, 0x82, &usb_return, sizeof(usb_return));
	} while (n_written != sizeof(usb_return));
}

static void cmd_echo(char *usb_data)
{
	uint8_t data[ECHO_MAX_DATA];
	uint16_t n_written;
	const uint8_t header_len = PACKET_DATA_START + 1;

	/* recv_counter already checked in usb_command_fsm */

	const uint8_t num_bytes = usb_data[PACKET_DATA_START];
	if (num_bytes > ECHO_MAX_DATA) {
		return;
	}

	for (int i = header_len; i < (header_len + num_bytes); i++) {
		data[i-header_len] = usb_data[i];
	}

	do {
		n_written = usbd_ep_write_packet(usbd_dev, 0x82, data, num_bytes);
	} while (n_written != num_bytes);
}

static void usb_command_fsm(char *usb_data)
{
	if (usb_data[PACKET_OPCODE] == CMD_RESET) {
		reset_send_counter();
		reset_recv_counter();
	}

	uint8_t expected = get_recv_counter();
	if (usb_data[PACKET_COUNTER] != expected) {
		usb_setting_return(USB_SETTING_ERROR);
		return;
	}

	switch(usb_data[PACKET_OPCODE])
	{
		/* Logic analyser related commands
		 * Note: changes to sample rate, capture length, acquisition mode,
		 * or channels in use are not permitted during data acquisition. */
		case CMD_SET_SAMPLE_RATE:
			if (data_acquire)
			{
				usb_setting_return(USB_SETTING_ERROR);
				return;
			}
			usb_setting_return(set_sample_rate(usb_data));
			break;
		case CMD_SET_CAPTURE_LENGTH:
			if (data_acquire)
			{
				usb_setting_return(USB_SETTING_ERROR);
				return;
			}
			usb_setting_return(set_capture_length(usb_data));
			break;
		case CMD_SEL_CHANNELS:
			if (data_acquire)
			{
				usb_setting_return(USB_SETTING_ERROR);
				return;
			}
			select_channels(usb_data);
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_BEGIN_ACQ:
			dma_init();
			uint8_t start_acq_result_code = start_acquisition();
			if (start_acq_result_code != USB_SETTING_SUCCESS) {
				usb_setting_return(start_acq_result_code);
			}
			if ((logic_config.porta_enabled > 0) && (logic_config.portb_enabled > 0))
			{
				data_acquire = true;
				gpio_channels_setup_a();
				gpio_channels_setup_b();
			}
			else if ((logic_config.porta_enabled == 0xff) && (logic_config.portb_enabled == 0))
			{
				data_acquire = true;
				gpio_channels_setup_a();
			}
			else if ((logic_config.porta_enabled == 0) && (logic_config.portb_enabled == 0xff))
			{
				data_acquire = true;
				gpio_channels_setup_b();
			}
			else {
				usb_setting_return(USB_SETTING_ERROR);
			}
			usb_setting_return(start_acq_result_code);
			break;
		case CMD_END_ACQ:
			stop_acquisition();
			usb_setting_return(USB_SETTING_SUCCESS);
			data_acquire = false;
			break;
		/* LED control commands */
		case CMD_LED_SET_RED:
			turn_led_red();
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_LED_SET_GREEN:
			turn_led_green();
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_LED_SET_BLUE:
			turn_led_blue();
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_LED_SET_OFF:
			turn_led_off();
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_PING:
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_RESET:
			stop_acquisition();
			data_acquire = false;
			reset_logic_state();
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		/*Function generator commands*/
		case CMD_CHOOSE_PINS_A:
			if (data_acquire && !logic_config.porta_enabled)
			{
				usb_setting_return(USB_SETTING_ERROR);
				return;
			}
			gpio_function_generator_setup_a();
			pins_choose_a(usb_data[PACKET_DATA_START]);
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_CHOOSE_PINS_B:
			if (data_acquire && !logic_config.portb_enabled)
			{
				usb_setting_return(USB_SETTING_ERROR);
				return;
			}
			gpio_function_generator_setup_b();
			pins_choose_b(usb_data[PACKET_DATA_START]);
			usb_setting_return(USB_SETTING_SUCCESS);
			break;
		case CMD_ECHO:
			cmd_echo(usb_data);
			/* no usb_setting_return because we're sending something
			   else instead */
			break;
	}
}

static void rcc_clock_setup_in_hse_12mhz_out_48mhz(void)
{
	rcc_osc_on(RCC_HSE);
	rcc_wait_for_osc_ready(RCC_HSE);
	rcc_set_sysclk_source(RCC_HSE);

	rcc_set_hpre(RCC_CFGR_HPRE_NODIV);
	rcc_set_ppre(RCC_CFGR_PPRE_NODIV);

	flash_prefetch_enable();
	flash_set_ws(FLASH_ACR_LATENCY_024_048MHZ);

	/* PLL: 12MHz * 4 = 48MHz */
	rcc_set_pll_multiplication_factor(RCC_CFGR_PLLMUL_MUL4);
	rcc_set_pll_source(RCC_CFGR_PLLSRC_HSE_CLK);
	rcc_set_pllxtpre(RCC_CFGR_PLLXTPRE_HSE_CLK);

	rcc_osc_on(RCC_PLL);
	rcc_wait_for_osc_ready(RCC_PLL);
	rcc_set_sysclk_source(RCC_PLL);

	rcc_apb1_frequency = 48000000;
	rcc_ahb_frequency = 48000000;
}

int main(void)
{
	rcc_clock_setup_in_hse_12mhz_out_48mhz();
	usb_setup();
	usbd_dev = usbd_init(&st_usbfs_double_usb_driver, &dev, &config, usb_strings,
		3, usbd_control_buffer, sizeof(usbd_control_buffer));
	usbd_register_set_config_callback(usbd_dev, cdcacm_set_config);

	led_setup();
	timer1_init();
	gpio_debug_setup();

	gpio_mode_setup(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO13);

	timer_enable_counter(TIM1);

	uint8_t *dma_buffer = NULL;

	while (true)
	{
		gpio_toggle(DEBUG_PINS_PORT, DEBUG_CH2);

		usbd_poll(usbd_dev);

		if(data_acquire)
		{
			if (logic_config.porta_enabled) {
				dma_buffer = dma_get_new_porta_buffer();
				if (dma_buffer) {
					usb_write_samples_double(0x82,
							PORTA_SAMPLES,
							dma_buffer);
					dma_release_porta_buffer();
				}
			}
			if (logic_config.portb_enabled) {
				dma_buffer = dma_get_new_portb_buffer();
				if (dma_buffer) {
					usb_write_samples_double(0x82,
							PORTB_SAMPLES,
							dma_buffer);
					dma_release_portb_buffer();
				}
			}
		}

		if (usb_host_status)
		{
			usb_host_status = 0;
			usb_command_fsm(usb_host_data);
		}
	}
}
