/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <counter.h>

static uint8_t send_counter = 0;
static uint8_t recv_counter = 0;

static uint8_t __get_counter(uint8_t *counter)
{
	uint8_t ret = *counter;

	if (*counter == UINT8_MAX) {
		*counter = 0;
	} else {
		*counter += 1;
	}

	return ret;
}

uint8_t get_send_counter(void)
{
	return __get_counter(&send_counter);
}

void reset_send_counter(void)
{
	send_counter = 0;
}

uint8_t get_recv_counter(void)
{
	return __get_counter(&recv_counter);
}

void reset_recv_counter(void)
{
	recv_counter = 0;
}

