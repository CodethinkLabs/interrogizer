/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

typedef struct {
	char sample_rate;
	bool sample_rate_set;
	uint64_t capture_quantity;
	uint8_t porta_enabled;
	uint8_t portb_enabled;
} logic_analyser_config_t;

extern logic_analyser_config_t logic_config;

/* Set sample rate for polling pin state */
/* Supported sample rates are predefined */
uint8_t set_sample_rate(char *usb_data);

/* Number of samples to collect before stopping */
uint8_t set_capture_length(char *usb_data);

/* Select channels to sample on */
void select_channels(char *usb_data);

/* Begin acquisition */
uint8_t start_acquisition(void);

/* End acquisition */
void stop_acquisition(void);

/* reset logic analyser state */
void reset_logic_state(void);
