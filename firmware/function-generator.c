/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <libopencm3/stm32/gpio.h>
#include <function-generator.h>

void pins_choose_a(uint8_t porta){
	gpio_clear(GPIOA, GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7);
	/*Read the current positions of the rest of port A*/
	uint16_t port_a_read = gpio_get(GPIOA, GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 |GPIO8 | GPIO9 | GPIO10 | GPIO11 | GPIO12 | GPIO13 | GPIO14 | GPIO15);

	/*Add the postions of Pins 0-7 wanted for port A while maintaining positions for the rest of port A*/
	uint16_t port_a_total = porta | port_a_read;

	gpio_port_write(GPIOA, port_a_total);
}

void pins_choose_b(uint8_t portb)
{
	gpio_clear(GPIOB, GPIO8 | GPIO9 | GPIO10 | GPIO11 | GPIO12 | GPIO13 | GPIO14 | GPIO15);
	/*Read the current positions of the rest of port B*/
	uint16_t port_b_read = gpio_get(GPIOB, GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 | GPIO9 | GPIO10 | GPIO11 | GPIO12 | GPIO13 | GPIO14 | GPIO15);

	/*Add the postions of Pins 8-15 wanted for port B while maintaining positions for the rest of port B*/
	uint16_t port_b_total = (portb << 8) | port_b_read;

	gpio_port_write(GPIOB, port_b_total);
}
