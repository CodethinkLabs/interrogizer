EESchema Schematic File Version 4
LIBS:Interrogizer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F0:STM32F072C8Tx U7
U 1 1 5E14EF46
P 7050 3900
F 0 "U7" H 7500 2450 50  0000 C CNN
F 1 "STM32F072C8Tx" H 6450 2450 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 6450 2500 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00090510.pdf" H 7050 3900 50  0001 C CNN
	1    7050 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5E14EFB7
P 1950 3200
F 0 "C9" H 2065 3246 50  0000 L CNN
F 1 "0.1uF" H 2065 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1988 3050 50  0001 C CNN
F 3 "~" H 1950 3200 50  0001 C CNN
	1    1950 3200
	1    0    0    -1  
$EndComp
Text HLabel 1750 1600 0    50   UnSpc ~ 0
V_DD
Text HLabel 1750 1700 0    50   UnSpc ~ 0
VDDIO2
Text HLabel 2450 1600 2    50   UnSpc ~ 0
VSS
Text Label 1800 1600 0    50   ~ 0
VDD
Wire Wire Line
	1800 1600 1750 1600
Text Label 1800 1700 0    50   ~ 0
VDDIO2
Text Label 2400 1600 2    50   ~ 0
VSS
Wire Wire Line
	2450 1600 2400 1600
Wire Wire Line
	1800 1700 1750 1700
Text Notes 2700 1250 2    50   ~ 0
Connect power to local symbols
Wire Notes Line
	1300 1800 2900 1800
Wire Notes Line
	2900 1100 1300 1100
Text Notes 2800 1450 2    50   ~ 0
(Allows use in designs with isolated \ngrounds)
Text Label 1950 2950 0    50   ~ 0
VDD
Text Label 1950 3450 2    50   ~ 0
VSS
Wire Wire Line
	1950 3450 1950 3350
Wire Wire Line
	1950 3050 1950 2950
$Comp
L Device:C C10
U 1 1 5E14F6FA
P 2400 3200
F 0 "C10" H 2515 3246 50  0000 L CNN
F 1 "0.1uF" H 2515 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2438 3050 50  0001 C CNN
F 3 "~" H 2400 3200 50  0001 C CNN
	1    2400 3200
	1    0    0    -1  
$EndComp
Text Label 2400 2950 0    50   ~ 0
VDD
Text Label 2400 3450 2    50   ~ 0
VSS
Wire Wire Line
	2400 3450 2400 3350
Wire Wire Line
	2400 3050 2400 2950
$Comp
L Device:C C8
U 1 1 5E14F780
P 2050 4000
F 0 "C8" H 2165 4046 50  0000 L CNN
F 1 "0.1uF" H 2165 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2088 3850 50  0001 C CNN
F 3 "~" H 2050 4000 50  0001 C CNN
	1    2050 4000
	1    0    0    -1  
$EndComp
Text Label 2050 3750 0    50   ~ 0
VDDA
Text Label 2050 4250 2    50   ~ 0
VSSA
Wire Wire Line
	2050 4250 2050 4150
Wire Wire Line
	2050 3850 2050 3750
$Comp
L Device:C C11
U 1 1 5E14F820
P 2850 3200
F 0 "C11" H 2965 3246 50  0000 L CNN
F 1 "0.1uF" H 2965 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2888 3050 50  0001 C CNN
F 3 "~" H 2850 3200 50  0001 C CNN
	1    2850 3200
	1    0    0    -1  
$EndComp
Text Label 2850 2950 0    50   ~ 0
VDDIO2
Text Label 2850 3450 2    50   ~ 0
VSS
Wire Wire Line
	2850 3450 2850 3350
Wire Wire Line
	2850 3050 2850 2950
$Comp
L Device:R R2
U 1 1 5E14FDA3
P 6150 2350
F 0 "R2" H 6220 2396 50  0000 L CNN
F 1 "10kR" H 6220 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6080 2350 50  0001 C CNN
F 3 "~" H 6150 2350 50  0001 C CNN
	1    6150 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2500 6150 2600
Wire Wire Line
	6150 2600 6350 2600
Text Label 6150 2100 0    50   ~ 0
VDD
Wire Wire Line
	6150 2100 6150 2200
Text Label 6950 2400 1    50   ~ 0
VDD
Text Label 7050 2400 1    50   ~ 0
VDD
Text Label 7150 2400 1    50   ~ 0
VDDA
Text Label 7250 2400 1    50   ~ 0
VDDIO2
Text Label 6850 5400 3    50   ~ 0
VSS
Text Label 6950 5400 3    50   ~ 0
VSS
Text Label 7050 5400 3    50   ~ 0
VSS
Text Label 7150 5400 3    50   ~ 0
VSSA
Text Label 6850 2400 1    50   ~ 0
VDD
$Comp
L Device:C C7
U 1 1 5E15B8F4
P 1500 3200
F 0 "C7" H 1615 3246 50  0000 L CNN
F 1 "0.1uF" H 1615 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1538 3050 50  0001 C CNN
F 3 "~" H 1500 3200 50  0001 C CNN
	1    1500 3200
	1    0    0    -1  
$EndComp
Text Label 1500 2950 0    50   ~ 0
VDD
Text Label 1500 3450 2    50   ~ 0
VSS
Wire Wire Line
	1500 3450 1500 3350
Wire Wire Line
	1500 3050 1500 2950
$Comp
L Device:Ferrite_Bead FB2
U 1 1 5E15C5A3
P 2850 4050
F 0 "FB2" V 3000 4000 50  0000 L CNN
F 1 "Ferrite_Bead" V 2700 3850 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 2780 4050 50  0001 C CNN
F 3 "~" H 2850 4050 50  0001 C CNN
	1    2850 4050
	0    -1   -1   0   
$EndComp
Text Label 2600 3950 2    50   ~ 0
VDDA
Wire Wire Line
	2600 4050 2700 4050
Text Label 3200 3950 2    50   ~ 0
VDD
Text Notes 2950 2800 2    50   ~ 0
These all need to be as close to \ntheir respective pins as possible
Text Notes 2900 4500 2    50   ~ 0
The decoupling cap for VDDA may\nwant to go to VSS instead of VSSA
Wire Notes Line
	1150 2550 1150 4550
Wire Notes Line
	1150 4550 3450 4550
Wire Notes Line
	3450 4550 3450 2550
Wire Notes Line
	3450 2550 1150 2550
Wire Notes Line
	1300 1100 1300 1800
Wire Notes Line
	2900 1100 2900 1800
$Comp
L Device:R_Small R1
U 1 1 5E15EB8D
P 5825 2925
F 0 "R1" V 5925 2925 50  0000 C CNN
F 1 "10kR" V 5720 2925 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5825 2925 50  0001 C CNN
F 3 "~" H 5825 2925 50  0001 C CNN
	1    5825 2925
	1    0    0    -1  
$EndComp
Text Label 5825 3175 2    50   ~ 0
VSS
Text HLabel 5550 2800 0    50   Input ~ 0
BOOT0
Wire Wire Line
	3200 4050 3200 3950
Wire Wire Line
	3000 4050 3200 4050
Wire Wire Line
	2600 4050 2600 3950
Wire Wire Line
	5825 3025 5825 3175
Text HLabel 8150 4800 2    50   BiDi ~ 0
USB_DM
Text HLabel 8150 4900 2    50   BiDi ~ 0
USB_DP
Wire Wire Line
	7650 4800 8150 4800
Wire Wire Line
	8150 4900 7650 4900
$Comp
L Device:C_Small C13
U 1 1 5E161E68
P 9550 2200
F 0 "C13" H 9642 2246 50  0000 L CNN
F 1 "30pF" H 9642 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9550 2200 50  0001 C CNN
F 3 "~" H 9550 2200 50  0001 C CNN
	1    9550 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 5E161EC6
P 8950 2200
F 0 "C12" H 9042 2246 50  0000 L CNN
F 1 "30pF" H 9042 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8950 2200 50  0001 C CNN
F 3 "~" H 8950 2200 50  0001 C CNN
	1    8950 2200
	1    0    0    -1  
$EndComp
Text Label 9550 2450 0    50   ~ 0
VSS
Text Label 8950 2450 0    50   ~ 0
VSS
Wire Wire Line
	8950 2450 8950 2300
Wire Wire Line
	9550 2450 9550 2300
Wire Wire Line
	9150 2050 8950 2050
Wire Wire Line
	8950 2050 8950 2100
Wire Wire Line
	9550 2100 9550 2050
Wire Wire Line
	9550 2050 9350 2050
Wire Wire Line
	8950 2050 8950 1700
Connection ~ 8950 2050
Wire Wire Line
	9550 2050 9550 1700
Connection ~ 9550 2050
Text Label 8950 1700 2    50   ~ 0
XTAL_IN
Text Label 9550 1700 0    50   ~ 0
XTAL_OUT
Text Label 6350 3000 2    50   ~ 0
XTAL_IN
Text Label 6350 3100 2    50   ~ 0
XTAL_OUT
Text Notes 8650 1550 0    50   ~ 0
If the high speed crystal isn't needed\nconnect these to hierarchical pins
Wire Notes Line
	8600 1300 8600 2500
Wire Notes Line
	8600 2500 10150 2500
Wire Notes Line
	10150 2500 10150 1300
Wire Notes Line
	10150 1300 8600 1300
Text Notes 4500 5250 2    50   ~ 0
I2S2_SD
Text Notes 4500 5150 2    50   ~ 0
I2S2_MCK
Text Notes 4500 5050 2    50   ~ 0
I2S2_CK
Text Notes 4500 4950 2    50   ~ 0
I2S2_WS
Text Notes 5050 4750 2    50   ~ 0
USART3_TX
Text Notes 5050 4850 2    50   ~ 0
USART3_RX
Text Notes 5050 4950 2    50   ~ 0
USART3_CK
Text Notes 5050 5050 2    50   ~ 0
USART3_CTS
Text Notes 5050 5150 2    50   ~ 0
USART3_RTS
Text Notes 5500 5050 2    50   ~ 0
I2C2_SCL
Text Notes 5500 5150 2    50   ~ 0
I2C2_SDA
Text Notes 5500 4750 2    50   ~ 0
I2C2_SCL
Text Notes 5500 4850 2    50   ~ 0
I2C2_SDA
Text Notes 5950 4750 2    50   ~ 0
SPI2_SCK
Text Notes 5950 4950 2    50   ~ 0
SPI2_NSS
Text Notes 5950 5050 2    50   ~ 0
SPI2_SCK
Text Notes 5950 5150 2    50   ~ 0
SPI2_MISO
Text Notes 5950 5250 2    50   ~ 0
SPI2_MOSI
Text Notes 5500 5400 2    50   ~ 0
Alternate pin functions:
Wire Notes Line
	4100 4750 6000 4750
Wire Notes Line
	4100 4850 6000 4850
Wire Notes Line
	6000 4950 4100 4950
Wire Notes Line
	4100 5050 6000 5050
Wire Notes Line
	6000 5150 4100 5150
Wire Notes Line
	6000 5250 4100 5250
Wire Notes Line
	4100 4650 6000 4650
Wire Notes Line
	4550 4650 4550 5250
Wire Notes Line
	5100 4650 5100 5250
Wire Notes Line
	5550 4650 5550 5250
Text HLabel 6350 4700 0    50   BiDi ~ 0
PB10
Text HLabel 6350 4800 0    50   BiDi ~ 0
PB11
Text HLabel 6350 4900 0    50   BiDi ~ 0
PB12
Text HLabel 6350 5000 0    50   BiDi ~ 0
PB13
Text HLabel 6350 5100 0    50   BiDi ~ 0
PB14
Text HLabel 6350 5200 0    50   BiDi ~ 0
PB15
Text HLabel 6350 4600 0    50   BiDi ~ 0
PB9
Text HLabel 6350 4500 0    50   BiDi ~ 0
PB8
Wire Notes Line
	4100 5450 6000 5450
Text Notes 4500 5650 0    50   ~ 0
These pins are used on the\npin header for comms.
Wire Notes Line
	4100 5700 6000 5700
Wire Notes Line
	4100 4650 4100 5700
Wire Notes Line
	6000 4650 6000 5700
Text Notes 8050 3700 0    50   ~ 0
ADC_IN0
Text Notes 8050 3800 0    50   ~ 0
ADC_IN1
Text Notes 8050 3900 0    50   ~ 0
ADC_IN2
Text Notes 8050 4000 0    50   ~ 0
ADC_IN3
Text Notes 8050 4100 0    50   ~ 0
ADC_IN4
Text Notes 8050 4200 0    50   ~ 0
ADC_IN5
Text Notes 8050 4300 0    50   ~ 0
ADC_IN6
Text Notes 8050 4400 0    50   ~ 0
ADC_IN7
Text Notes 8400 4100 0    50   ~ 0
DAC_OUT1
Text Notes 8400 4200 0    50   ~ 0
DAC_OUT2
Text Notes 5900 5850 2    50   ~ 0
Pins PB8 through PB15 are all 5V tolerant
Text HLabel 7650 3700 2    50   BiDi ~ 0
PA0
Text HLabel 7650 3800 2    50   BiDi ~ 0
PA1
Text HLabel 7650 3900 2    50   BiDi ~ 0
PA2
Text HLabel 7650 4000 2    50   BiDi ~ 0
PA3
Text HLabel 7650 4100 2    50   BiDi ~ 0
PA4
Text HLabel 7650 4200 2    50   BiDi ~ 0
PA5
Text HLabel 7650 4300 2    50   BiDi ~ 0
PA6
Text HLabel 7650 4400 2    50   BiDi ~ 0
PA7
Text HLabel 7650 5000 2    50   BiDi ~ 0
SWDIO
Text HLabel 7650 5100 2    50   BiDi ~ 0
SWCLK
Text HLabel 5550 2600 0    50   Input ~ 0
~RST
Wire Wire Line
	5550 2600 6150 2600
Connection ~ 6150 2600
Text Notes 8950 3700 0    50   ~ 0
USART2_CTS
Text Notes 8950 3800 0    50   ~ 0
USART2_RTS
Text Notes 8950 3900 0    50   ~ 0
USART2_TX
Text Notes 8950 4000 0    50   ~ 0
USART2_RX
Text Notes 8950 4100 0    50   ~ 0
USART2_CK
Wire Notes Line
	9300 3550 9450 3550
Wire Notes Line
	9450 3550 9450 4150
Wire Notes Line
	9450 4150 9300 4150
Wire Notes Line
	9450 3850 9600 3850
Text Notes 9650 3950 0    50   ~ 0
The USART on this side support\nmore features (e.g LIN and IRDA)\nbut lack 5V tolerance
Text HLabel 6350 3300 0    50   BiDi ~ 0
PC13
Text HLabel 6350 3400 0    50   BiDi ~ 0
PC14
Text HLabel 6350 3500 0    50   BiDi ~ 0
PC15
Text HLabel 6350 3700 0    50   BiDi ~ 0
PB0
Text HLabel 6350 3800 0    50   BiDi ~ 0
PB1
Text HLabel 6350 3900 0    50   BiDi ~ 0
PB2
Text HLabel 6350 4000 0    50   BiDi ~ 0
PB3
Text HLabel 6350 4100 0    50   BiDi ~ 0
PB4
Text HLabel 6350 4200 0    50   BiDi ~ 0
PB5
Text HLabel 6350 4300 0    50   BiDi ~ 0
PB6
Text HLabel 6350 4400 0    50   BiDi ~ 0
PB7
Wire Notes Line
	6100 3650 6100 4450
Wire Notes Line
	6100 4450 6200 4450
Wire Notes Line
	6100 3650 6200 3650
Wire Notes Line
	5950 4050 6100 4050
Text Notes 5900 4050 2    50   ~ 0
Spare I/O
Text HLabel 7650 5200 2    50   BiDi ~ 0
PA15
Text HLabel 7650 4500 2    50   BiDi ~ 0
PA8
Text HLabel 7650 4600 2    50   BiDi ~ 0
PA9
Text HLabel 7650 4700 2    50   BiDi ~ 0
PA10
Wire Notes Line
	7800 4450 7950 4450
Wire Notes Line
	7950 4450 7950 4750
Wire Notes Line
	7950 4750 7800 4750
Wire Notes Line
	7950 4600 8150 4600
Text Notes 8200 4600 0    50   ~ 0
Spare I/O
Wire Notes Line
	7950 5200 8150 5200
Text Notes 8200 5200 0    50   ~ 0
Spare I/O
Wire Notes Line
	6150 3250 6050 3250
Wire Notes Line
	6050 3250 6050 3550
Wire Notes Line
	6050 3550 6150 3550
Text Notes 5900 3400 2    50   ~ 0
Spare I/O
Wire Notes Line
	5950 3400 6050 3400
$Comp
L Device:C C14
U 1 1 5E1CD973
P 1500 4000
F 0 "C14" H 1615 4046 50  0000 L CNN
F 1 "10nF" H 1615 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1538 3850 50  0001 C CNN
F 3 "~" H 1500 4000 50  0001 C CNN
	1    1500 4000
	1    0    0    -1  
$EndComp
Text Label 1500 3750 0    50   ~ 0
VDDA
Text Label 1500 4250 2    50   ~ 0
VSSA
Wire Wire Line
	1500 4250 1500 4150
Wire Wire Line
	1500 3850 1500 3750
$Comp
L Interrogizer-rescue:Crystal_GND24_Small-Device-USB_Comms_Bridge-rescue Y1
U 1 1 5E2A6AEE
P 9250 2050
AR Path="/5E2A6AEE" Ref="Y1"  Part="1" 
AR Path="/5E14EF23/5E2A6AEE" Ref="Y1"  Part="1" 
F 0 "Y1" H 9391 2096 50  0000 L CNN
F 1 "Crystal_GND24_Small" H 9391 2005 50  0000 L CNN
F 2 "rounded-pads-footprints:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 9250 2050 50  0001 C CNN
F 3 "~" H 9250 2050 50  0001 C CNN
	1    9250 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E2A6BC6
P 9250 2250
F 0 "#PWR03" H 9250 2000 50  0001 C CNN
F 1 "GND" H 9255 2077 50  0000 C CNN
F 2 "" H 9250 2250 50  0001 C CNN
F 3 "" H 9250 2250 50  0001 C CNN
	1    9250 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2175 9250 2250
$Comp
L power:GND #PWR04
U 1 1 5E2A7C98
P 9375 1750
F 0 "#PWR04" H 9375 1500 50  0001 C CNN
F 1 "GND" H 9380 1577 50  0000 C CNN
F 2 "" H 9375 1750 50  0001 C CNN
F 3 "" H 9375 1750 50  0001 C CNN
	1    9375 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1725 9375 1725
Wire Wire Line
	9250 1725 9250 1925
Wire Wire Line
	9375 1750 9375 1725
Wire Wire Line
	5550 2800 5825 2800
Wire Wire Line
	5825 2825 5825 2800
Connection ~ 5825 2800
Wire Wire Line
	5825 2800 6350 2800
Text Label 1725 2200 0    50   ~ 0
VSS
Text Label 2150 2200 0    50   ~ 0
VSSA
Wire Wire Line
	1725 2200 2150 2200
$EndSCHEMATC
