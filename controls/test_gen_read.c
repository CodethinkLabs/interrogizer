/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "usb_protocol.h"
#include <string.h>
#include <unistd.h>
#include "common.h"
#include "usb_protocol.h"
#include <errno.h>
#include <stdbool.h>

#define SAMPLE_RATE 	SAMPLE_200KHZ

enum port {
	PORTA,
	PORTB,
};

static int get_data(int fd, enum port input_dir, uint8_t expected)
{
	ssize_t n_read;
	uint8_t *buf;
	int ret = EXIT_FAILURE;
	unsigned i;

	buf = malloc(MAX_PACKET_LEN);
	if (!buf) {
		return EXIT_FAILURE;
	}

	n_read = read(fd, buf, MAX_PACKET_LEN);
	if (n_read < 0) {
		fprintf(stderr, "Error reading: %s\n", strerror(errno));
		goto out;
	} else if (n_read == 0) {
		fprintf(stderr, "Unexpected EOF\n");
		goto out;
	} else if (n_read != MAX_PACKET_LEN) {
		fprintf(stderr, "Partial packet read: %li/%li\n",(long) n_read, (long) MAX_PACKET_LEN);
		fprintf(stderr, "I read: ");
		for (unsigned i = 0; i < n_read; i++) {
			fprintf(stderr, "0x%x ", buf[i]);
		}
		fprintf(stderr, "\n");
		goto out;
	}

	if (input_dir == PORTA && buf[PACKET_OPCODE] != PORTA_SAMPLES) {
		fprintf(stderr, "Expected port A samples, got something else\n");
		goto out;
	} else if (input_dir == PORTB && buf[PACKET_OPCODE] != PORTB_SAMPLES) {
		fprintf(stderr, "Expected port B samples, got something else\n");
		goto out;
	}

	/* check receive counter */
	uint8_t expected_rcv_cnt = get_rcv_counter();
	if (buf[PACKET_COUNTER] != expected_rcv_cnt) {
		fprintf(stderr, "Receive counter missmatch: expected %i got %i\n",
			expected_rcv_cnt, buf[1]);
		goto out;
	}

	for (i = PACKET_DATA_START; i < MAX_PACKET_LEN; i++) {
		if (buf[i] != expected){
			fprintf(stderr, "sample %i: expected 0x%x, got 0x%x\n",
					i, expected, buf[i]);
			goto out;
		}
	}
	ret = EXIT_SUCCESS;

out:
	free(buf);
	return ret;
}

int test_direction(int fd, enum port input_dir)
{
	uint8_t cmd[3];
	int ret, old_ret;

	reset_counters();
	cmd[PACKET_OPCODE] = CMD_RESET;
	ret = send_command(fd, cmd, 1);
	if (ret != EXIT_SUCCESS) {
		return ret;
	}

	/* acquire samples only on port a */
	cmd[PACKET_OPCODE] = CMD_SEL_CHANNELS;
	if (input_dir == PORTA) {
		cmd[1] = 0xff;
		cmd[2] = 0x00;
	} else {
		cmd[1] = 0x00;
		cmd[2] = 0xff;
	}
	ret = send_command(fd, cmd, 3);
	if (ret != EXIT_SUCCESS) {
		return ret;
	}

	/* set capture length (we only collect a single packet anyway) */
	ret = set_capture_length(fd, MIN_SAMPLE_LEN);
	if (ret != EXIT_SUCCESS) {
		return ret;
	}

	/* set sample rate */
	cmd[PACKET_OPCODE] = CMD_SET_SAMPLE_RATE;
	cmd[1] = SAMPLE_RATE;
	ret = send_command(fd, cmd, 2);
	if (ret != EXIT_SUCCESS) {
		return ret;
	}

	/* Cycles through all pin output settings for the output port */
	for (int i = 0; i < 256; i++)
	{
		if (input_dir == PORTA) {
			cmd[0] = CMD_CHOOSE_PINS_B;
		} else {
			cmd[0] = CMD_CHOOSE_PINS_A;
		}
		cmd[1] = i;
		ret = send_command(fd, cmd, 2);
		if (ret != EXIT_SUCCESS) {
			break;
		}

		cmd[0] = CMD_BEGIN_ACQ;
		ret = send_command(fd, cmd, 1);
		if (ret != EXIT_SUCCESS) {
			break;
		}

		old_ret = get_data(fd, input_dir, i);

		/* Stops reading from port */
		cmd[0] = CMD_END_ACQ;
		ret = send_command(fd, cmd, 1);
		if (ret != EXIT_SUCCESS) {
			break;
		}

		ret = old_ret;
		if (ret != EXIT_SUCCESS) {
			printf("packet %d failed\n", i);
			break;
		}
	}

	old_ret = ret;

	/* turn off port output */
	if (input_dir == PORTA) {
		cmd[0] = CMD_CHOOSE_PINS_B;
	} else {
		cmd[0] = CMD_CHOOSE_PINS_A;
	}
	cmd[1] = 0;
	ret = send_command(fd, cmd, 2);
	if (ret != EXIT_SUCCESS) {
		return ret;
	}

	return old_ret;
}

int main(int argc, char *argv[])
{
	int fd;
	uint8_t cmd;
	int ret;

	/* Open the port file */
	fd = proc_args(argc, argv);
	if (fd == -1) {
		return EXIT_FAILURE;
	}

	ret = test_direction(fd, PORTA);
	if (ret != EXIT_SUCCESS) {
		goto out;
	}

	ret = test_direction(fd, PORTB);
	if (ret != EXIT_SUCCESS) {
		goto out;
	}

	cmd = CMD_RESET;
	ret = send_command(fd, &cmd, 1);

out:
	close(fd);

	if (ret == EXIT_SUCCESS) {
		printf("Test success\n");
	} else {
		printf("Test failure\n");
	}

	return ret;
}
