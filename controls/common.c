/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#include "common.h"
#include "usb_protocol.h"

#define DEFAULT_PORT "/dev/ttyACM0"
#define RESP_TIMEOUT_SEC 1

static uint8_t rcv_counter = 0;
static uint8_t snd_counter = 0;

enum command_args {
	PROG,
	SERIAL_PORT,
	NUM_ARGS,
};

static void print_usage(FILE *stream, const char *prog)
{
	fprintf(stream, "Usage:\n");
	fprintf(stream, "%s [SERIAL_PORT]\n", prog);
	fprintf(stream, "\tSERIAL_PORT\tDefaults to %s\n", DEFAULT_PORT);
}

/* return same as open() */
int open_tty(char *serial_port)
{
	int fd;
	struct termios t;
	uint8_t cmd;

	fd = open(serial_port, O_RDWR);
	if (fd == -1) {
		fprintf(stderr, "Cannot open '%s': %s\n", serial_port,
			strerror(errno));
		return -1;
	}

	if (tcgetattr(fd, &t) != 0) {
		fprintf(stderr, "tcgetattr(): %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	/* raw mode terminal without parity checks or NL/CR fiddling */
	t.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	t.c_oflag &= ~(OPOST);
	t.c_cflag |=  (CS8);
	t.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

	if (tcsetattr(fd, TCSANOW, &t) != 0) {
		fprintf(stderr, "tcsetattr(): %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	/* check that we are actually talking to an interrogizer by resetting it */
	cmd = CMD_RESET;
	if (send_command(fd, &cmd, sizeof(cmd)) != EXIT_SUCCESS) {
		close(fd);
		return -1;
	}

	return fd;
}

/* processes command line arguments and spits out the serial port */
int proc_args(int argc, char **argv)
{
	char *serial_port;
	int port_file;

	if (argc > NUM_ARGS) {
		print_usage(stderr, argv[PROG]);
		exit(EXIT_FAILURE);
	}

	if (argc == NUM_ARGS) {
		if (strcmp("--help", argv[1]) == 0 || strcmp("-h", argv[1]) == 0) {
			print_usage(stdout, argv[PROG]);
			exit(EXIT_SUCCESS);
		}

		serial_port = argv[SERIAL_PORT];
	} else {
		serial_port = DEFAULT_PORT;
	}

	port_file = open_tty(serial_port);
	if (port_file == -1) {
		exit(EXIT_FAILURE);
	}

	return port_file;
}

int write_byte(int fd, uint8_t byte)
{
	ssize_t n_written;

	n_written = write(fd, &byte, sizeof(byte));
	if (n_written != 1) {
		fprintf(stderr, "Error writing response to terminal: %s\n",
			strerror(errno));
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/* return value same as for read() */
ssize_t read_byte(int fd, uint8_t *buf)
{
	ssize_t n_read;

	for (;;) {
		errno = 0;
		n_read = read(fd, buf, sizeof(*buf));
		if (n_read == sizeof(*buf)) {
			return sizeof(*buf);
		} else if (n_read == 0) {
			/* EOF */
			return 0;
		} else if (n_read == -1 && (errno == EIO || errno == EWOULDBLOCK)) {
			/* other side disconnected. Try again after sleep */
			usleep(10);
		} else {
			fprintf(stderr, "Error reading from serial: %s\n",
					strerror(errno));
			return -1;
		}
	}
}

static void alarm_handler(int signum)
{
	(void) signum;

	fprintf(stderr, "Timeout reading response\n");
	fprintf(stderr, "Did you choose the wrong ttyACM device?\n");
	exit(EXIT_FAILURE);
}

static int get_response(int fd, uint8_t cmd)
{
	int ret;
	uint8_t resp;
	ssize_t n_read;
	struct sigaction oldact;
	struct sigaction act;
	/* enough for the samples plus packet counter byte */
	static uint8_t junk_buffer[NUM_SAMPLES + 1];

	/* set a timeout for RESP_TIMEOUT_SEC so we don't wait forever for a
	   dead device */
	memset(&act, 0, sizeof(act));
	act.sa_handler = alarm_handler;
	if (sigaction(SIGALRM, &act, &oldact) != 0) {
		fprintf(stderr, "sigaction(): %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	alarm(RESP_TIMEOUT_SEC);

	/* attempt to read the response */
	do {
		n_read = read_byte(fd, &resp);
		if (n_read != 1) {
			ret = EXIT_FAILURE;
			goto out;
		}

		switch (resp) {
		case USB_SETTING_SUCCESS:
			ret = EXIT_SUCCESS;
			goto out;
		case USB_SETTING_ERROR:
			fprintf(stderr, "Received USB_SETTING_ERROR while processing 0x%x\n",
					cmd);

			ret = EXIT_FAILURE;
			goto out;
		case PORTA_SAMPLES:
			/* fallthrough */
		case PORTB_SAMPLES:
			if (cmd == CMD_END_ACQ) {
				/* just ignore the sample packet here and try
				   again after flushing the sample data */
				n_read = read(fd, &junk_buffer, NUM_SAMPLES + 1);
				get_rcv_counter();
				if (n_read != NUM_SAMPLES + 1) {
					fprintf(stderr, "Unexpected n_read = %li while reading junk buffer\n",
							n_read);
					fprintf(stderr, "I read: ");
					for (unsigned i = 0; i < n_read; i++) {
						fprintf(stderr, "0x%x ", junk_buffer[i]);
					}
					fprintf(stderr, "\n");

					ret = EXIT_FAILURE;
					goto out;
				}
				continue;
			}
			/* otherwise we shouldn't have gotten a sample packet */
			/* fallthrough */
		default:
			fprintf(stderr, "Unknown response 0x%x for cmd 0x%x\n",
					(int)resp, cmd);
			ret = EXIT_FAILURE;
			goto out;
		}
	} while (1);

out:
	/* cancel alarm */
	alarm(0);

	/* reset to old signal handler */
	if (sigaction(SIGALRM, &oldact, NULL) != 0) {
		fprintf(stderr, "sigaction(): %s\n", strerror(errno));
		return EXIT_FAILURE;
	}
	return ret;
}

int send_command(int fd, uint8_t *buf, size_t len)
{
	ssize_t n_written;
	uint8_t bigger_buf[MAX_PACKET_LEN];

	if (buf[PACKET_OPCODE] == CMD_RESET) {
		/* CMD_RESET shoud always have counter = 0 so that both sides
		   can sync up their numbers */
		reset_counters();
	}

	/* -1 so we can add the counter byte */
	if (len > MAX_PACKET_LEN-1) {
		fprintf(stderr, "Packet length %li too long: maximum is %i\n",
				len, MAX_PACKET_LEN-1);
		return EXIT_FAILURE;
	}

	bigger_buf[PACKET_OPCODE] = buf[PACKET_OPCODE];
	bigger_buf[PACKET_COUNTER] = get_snd_counter();

	for (unsigned i = PACKET_DATA_START - 1; i < len; i++) {
		/* +1 is because we added the counter */
		bigger_buf[i+1] = buf[i];
	}

	n_written = write(fd, bigger_buf, len+1);
	if (n_written < 0 || (size_t) n_written != len+1) {
		fprintf(stderr, "n_written = %lli\n", (long long) n_written);
		fprintf(stderr, "Error writing to serial: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	return get_response(fd, buf[PACKET_OPCODE]);
}

/* to save typing */
void assert_command(int fd, uint8_t *buf, size_t len)
{
	if (send_command(fd, buf, len) != EXIT_SUCCESS) {
		exit(EXIT_FAILURE);
	}
}

void assert_short_command(int fd, uint8_t cmd) {
	assert_command(fd, &cmd, 1);
}

int set_capture_length(int fd, uint64_t capture_length)
{
	uint8_t cmd[9];
	unsigned i;

	if (capture_length < MIN_SAMPLE_LEN || capture_length > MAX_SAMPLE_LEN) {
		fprintf(stderr, "%lu is out of range\n", (long unsigned) capture_length);
		return EXIT_FAILURE;
	}

	cmd[0] = CMD_SET_CAPTURE_LENGTH;
	/* big endian */
	for (i = 1; i <= 8; i++) {
		cmd[i] = (capture_length >> (8 * (8 - i))) & 0xff;
	}

	return send_command(fd, cmd, 9);
}

uint8_t __get_counter(uint8_t *counter)
{
	uint8_t ret = *counter;

	if (*counter == UINT8_MAX) {
		*counter = 0;
	} else {
		*counter += 1;
	}

	return ret;
}

uint8_t get_rcv_counter(void)
{
	return __get_counter(&rcv_counter);
}

uint8_t get_snd_counter(void)
{
	return __get_counter(&snd_counter);
}

void reset_counters(void)
{
	rcv_counter = 0;
	snd_counter = 0;
}
