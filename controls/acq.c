/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "usb_protocol.h"

#define SAMPLE_RATE        SAMPLE_10KHZ
#define CAPTURE_LENGTH 10000 /* 10k */
#define PORTA 0xff
#define PORTB 0xff

static void setup(int fd, uint64_t capture_length, uint8_t sample_rate,
		uint8_t porta, uint8_t portb)
{
	uint8_t cmd[3];

	if (set_capture_length(fd, capture_length) != EXIT_SUCCESS) {
		exit(EXIT_FAILURE);
	}

	/* packet counters added in assert_command */

	cmd[0] = CMD_SET_SAMPLE_RATE;
	cmd[1] = sample_rate;
	assert_command(fd, cmd, 2);

	cmd[0] = CMD_SEL_CHANNELS;
	cmd[1] = porta;
	cmd[2] = portb;
	assert_command(fd, cmd, 3);

	assert_short_command(fd, CMD_BEGIN_ACQ);
}

static int get_data(int fd, ssize_t capture_length, uint8_t porta, uint8_t portb)
{
	ssize_t porta_remaining, portb_remaining, n_read;
	uint8_t *buf;
	int ret = EXIT_FAILURE;

	buf = malloc(MAX_PACKET_LEN);
	if (!buf) {
		return EXIT_FAILURE;
	}

	porta_remaining = porta ? capture_length : 0;
	portb_remaining = portb ? capture_length : 0;

	while ((porta_remaining > 0) || (portb_remaining > 0)) {
		n_read = read(fd, buf, MAX_PACKET_LEN);
		if (n_read < 0) {
			fprintf(stderr, "Error reading: %s\n", strerror(errno));
			goto out;
		} else if (n_read == 0) {
			fprintf(stderr, "Unexpected EOF\n");
			goto out;
		} else if (n_read != MAX_PACKET_LEN) {
			fprintf(stderr, "Partial read: %li/%li\n",
				(long) n_read, (long) MAX_PACKET_LEN);
			goto out;
		}

		switch (buf[PACKET_OPCODE]) {
		case PORTA_SAMPLES:
			porta_remaining -= NUM_SAMPLES;
			printf("Port A ");
			break;
		case PORTB_SAMPLES:
			portb_remaining -= NUM_SAMPLES;
			printf("Port B ");
			break;
		default:
			fprintf(stderr, "Unknown op-code 0x%x, expected samples\n",
					(int) buf[0]);
			goto out;
		}

		/* check receive counter */
		uint8_t expected_rcv_cnt = get_rcv_counter();
		if (buf[PACKET_COUNTER] != expected_rcv_cnt) {
			fprintf(stderr, "Receive counter missmatch: expected %i got %i\n",
					expected_rcv_cnt, buf[PACKET_COUNTER]);
			goto out;
		}

		/* just print it out */
		printf("data packet: ");
		for (unsigned i = PACKET_DATA_START; i < MAX_PACKET_LEN; i++) {
			printf("%02hhx ", buf[i]);
		}
		printf("\n");
	}

	ret = EXIT_SUCCESS;

out:
	free(buf);
	return ret;
}

static void stop(int fd)
{
	assert_short_command(fd, CMD_END_ACQ);
}

int main(int argc, char **argv)
{
	int ret;
	int port_file;
	port_file = proc_args(argc, argv);

	setup(port_file, CAPTURE_LENGTH, SAMPLE_RATE, PORTA, PORTB);
	ret = get_data(port_file, CAPTURE_LENGTH, PORTA, PORTB);
	stop(port_file);

	close(port_file);
	return ret;
}
