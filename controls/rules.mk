# Variables to define
# - BUILD_DIR
# - BIN_DIR (must not contain anything else)
# - PROJ_DIRS
# - COMMON_DEPS (optional)
# - BINFILES

CC = gcc
CFLAGS ?= -O2 -Wall -Wextra -Wpedantic -Werror
SCAN_BUILD ?= $(shell ./detect-scan-build.sh)

DEBUG_FLAGS ?= -g -fsanitize=address -fsanitize=undefined -fstack-protector-all
RELEASE_FLAGS ?=

INCLUDE += ../common
CFLAGS += -I$(INCLUDE)

# `RELEASE=1 make`
ifeq ($(RELEASE), 1)
	CFLAGS += $(RELEASE_FLAGS)
	SCAN_BUILD =
else
	CFLAGS += $(DEBUG_FLAGS)
endif

LDFLAGS := $(CFLAGS)

SRCFILES := $(shell find $(PROJ_DIRS) -type f -name "*.c")
HDRFILES := $(shell find $(PROJ_DIRS) -type f -name "*.h")
OBJFILES := $(patsubst %.c,$(BUILD_DIR)/%.o,$(SRCFILES))
DEPFILES := $(patsubst %.c,$(BUILD_DIR)/%.o.d,$(SRCFILES))
TARGETS  := $(patsubst %,$(BIN_DIR)/%,$(BINFILES))

.PHONY: all clean
all: tags $(TARGETS)

-include $(DEPFILES)

$(BUILD_DIR):
	mkdir -p $@

$(BIN_DIR):
	mkdir -p $@

$(BUILD_DIR)/%.o: %.c $(BUILD_DIR) Makefile rules.mk
	$(SCAN_BUILD) $(CC) $(CFLAGS) -MMD -MP -MF $@.d -c $< -o $@

$(BIN_DIR)/%: $(BUILD_DIR)/%.o $(BIN_DIR) $(COMMON_DEPS)
	$(CC) $(LDFLAGS) $< $(COMMON_DEPS) -o $@

tags: $(SRCFILES) $(HDRFILES)
	ctags -R

clean:
	-@rm -r $(wildcard $(BIN_DIR) $(BUILD_DIR) tags)
