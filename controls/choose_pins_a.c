/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "usb_protocol.h"
#include <string.h>
#include <unistd.h>

enum command_args {
	PROG,
	SERIAL_PORT,
	PORT_A,
	NUM_ARGS,
};

static int usage_message(char *prog)
{
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "%s <SERIAL_PORT> <PINS_PORT_B> \n", prog);
	return EXIT_FAILURE;
}

int main(int argc, char *argv[])
{
	uint8_t cmd[2];
	int ret;
	uint8_t porta = 0;
	char *enda = NULL;

	if (argc != NUM_ARGS)
	{
		return usage_message(argv[PROG]);
	}

	/* Open the port file */
	int port_file;
	port_file = open_tty(argv[SERIAL_PORT]);

	if (port_file == -1) 
	{
		return EXIT_FAILURE;
	}



	if(strlen(argv[PORT_A]) != 8 && strlen(argv[PORT_A]) != 1)
	{
		return usage_message(argv[PROG]);
	}
	else if (strlen(argv[PORT_A]) == 1)
	{
		if (strcmp(argv[PORT_A], "0") == 0)
		{
			porta = 0;
			ret = EXIT_SUCCESS;
		}
		else if (strcmp(argv[PORT_A], "1") == 0)
		{
			porta = 0xff;
			ret = EXIT_SUCCESS;
		}
		else
		{
			return usage_message(argv[PROG]);
		}
	}
	else
	{
		porta  = strtoul(argv[PORT_A], &enda, 2);
		if (((enda != NULL && *enda == '\0') == 0))
		{
			return usage_message(argv[PROG]);
		}
		else
		{
			ret = EXIT_SUCCESS;
		}
	}

	cmd[0] = CMD_CHOOSE_PINS_A;
	cmd[1] = porta;
	assert_command(port_file, cmd, 2);
	close(port_file);
	return ret;
}
