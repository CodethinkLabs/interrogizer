/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int open_tty(char *serial_port);
int proc_args(int argc, char **argv);

int write_byte(int fd, uint8_t byte);
ssize_t read_byte(int fd, uint8_t *buf);

int send_command(int fd, uint8_t *buf, size_t len);
void assert_command(int fd, uint8_t *buf, size_t len);
void assert_short_command(int fd, uint8_t cmd);

int set_capture_length(int fd, uint64_t capture_length);

uint8_t __get_counter(uint8_t *counter);
uint8_t get_rcv_counter(void);
uint8_t get_snd_counter(void);
void reset_counters(void);

#endif /* COMMON_H */
