/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "usb_protocol.h"

#define NUM_RUNS 100

/* Size of data portion of the echo packet:
   - PACKET_DATA_START - 1, to include space for the standard command header and
   the length byte */
#define MAX_DATA_LEN (MAX_PACKET_LEN - PACKET_DATA_START - 1)

static void echo_command(int fd, const uint8_t *restrict bytes, const uint8_t len)
{
	uint8_t packet[MAX_PACKET_LEN];
	uint8_t rcved[MAX_DATA_LEN];
	ssize_t n_written, n_read;
	const ssize_t n_to_write = PACKET_DATA_START + 1 + len;


	if (len > MAX_DATA_LEN) {
		return;
	}

	packet[PACKET_OPCODE] = CMD_ECHO;
	packet[PACKET_COUNTER] = get_snd_counter();
	packet[PACKET_DATA_START] = len;

	fprintf(stderr, "Sending CMD_ECHO 0x%x ", len);
	for (unsigned i = 0; i < len; i++) {
		packet[PACKET_DATA_START + 1 +i] = bytes[i];
		fprintf(stderr, "0x%x ", bytes[i]);
	}
	fprintf(stderr, "\n");

	n_written = write(fd, packet, n_to_write);
	if (n_written == -1) {
		fprintf(stderr, "Error writing echo command: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	} else if (n_written != n_to_write) {
		fprintf(stderr, "Partial write: %li/%li\n", (long) n_written,
				n_to_write);
		exit(EXIT_FAILURE);
	}

	/* read echo response */
	n_read = read(fd, rcved, MAX_DATA_LEN);
	if (n_read == -1) {
		fprintf(stderr, "Error reading echo response: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	fprintf(stderr, "I read ");
	for (ssize_t i = 0; i < n_read; i++) {
		fprintf(stderr, "0x%x ", rcved[i]);
	}
	fprintf(stderr, "\n");

	if (n_read != len) {
		fprintf(stderr, "Unexpected read length: %li/%u\n",
				(long) n_read, len);
		exit(EXIT_FAILURE);
	}

	for (unsigned i = 0; i < len; i++) {
		if (rcved[i] != bytes[i]) {
			fprintf(stderr, "Error at byte %u: expected 0x%x got 0x%x\n",
					i, bytes[i], rcved[i]);
			exit(EXIT_FAILURE);
		}
	}
}

static void test_packet(int fd, uint8_t length)
{
	uint8_t bytes[MAX_DATA_LEN];

	if (length > MAX_DATA_LEN) {
		return;
	}

	for (unsigned i = 0; i < length; i++) {
		bytes[i] = (uint8_t) rand() % 0xff;
	}

	echo_command(fd, bytes, length);
}

int main(int argc, char *argv[])
{
	int fd;

	fd = proc_args(argc, argv);
	if (fd == -1) {
		return EXIT_FAILURE;
	}

	for (unsigned runs = 0; runs < NUM_RUNS; runs++) {
		for (unsigned len = 1; len <= MAX_DATA_LEN; len++) {
			printf("Trying length=%u\n", len);
			test_packet(fd, len);
		}
	}

	return EXIT_SUCCESS;
}
