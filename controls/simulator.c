/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _XOPEN_SOURCE 600
#define _DEFAULT_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include "usb_protocol.h"
#include "common.h"

#define DEFAULT_SAMPLE_LEN 100
#define DEFAULT_PORT_A_MASK 0xff
#define DEFAULT_PORT_B_MASK 0xff

struct simulator {
	/* uint64_t sample_rate; - not simulating this for now */
	uint64_t sample_length;
	uint8_t porta;
	uint8_t portb;
	bool capturing;
	/* number of bytes waiting to be sent */
	uint64_t usb_data_counter;
	/* number of samples sent in this session */
	uint64_t data_points_sampled;
	bool porta_output;
	bool portb_output;
	uint8_t buf[MAX_PACKET_LEN];
	uint8_t send_counter;
	uint8_t recv_counter;
};

static int set_sample_rate(int fd)
{
	uint8_t arg;
	char *sample_rate;

	if (read_byte(fd, &arg) != sizeof(arg)) {
		return EXIT_FAILURE;
	}

	switch (arg) {
	case SAMPLE_200KHZ:
		sample_rate = "200kHz";
		break;
	case SAMPLE_100KHZ:
		sample_rate = "100kHz";
		break;
	case SAMPLE_50KHZ:
		sample_rate = "50kHz";
		break;
	case SAMPLE_20KHZ:
		sample_rate = "20kHz";
		break;
	case SAMPLE_10KHZ:
		sample_rate = "10kHz";
		break;
	case SAMPLE_5KHZ:
		sample_rate = "5kHz";
		break;
	case SAMPLE_2KHZ:
		sample_rate = "2kHz";
		break;
	case SAMPLE_1KHZ:
		sample_rate = "1kHz";
		break;
	case SAMPLE_500HZ:
		sample_rate = "500Hz";
		break;
	case SAMPLE_200HZ:
		sample_rate = "200Hz";
		break;
	case SAMPLE_100HZ:
		sample_rate = "100Hz";
		break;
	case SAMPLE_50HZ:
		sample_rate = "50Hz";
		break;
	case SAMPLE_20HZ:
		sample_rate = "20Hz";
		break;
	case SAMPLE_10HZ:
		sample_rate = "10Hz";
		break;
	case SAMPLE_5HZ:
		sample_rate = "5Hz";
		break;
	case SAMPLE_2HZ:
		sample_rate = "2Hz";
		break;
	case SAMPLE_1HZ:
		sample_rate = "1Hz";
		break;
	default:
		fprintf(stderr, "Unknown sample rate 0x%x\n", (int) arg);
		return EXIT_FAILURE;
	}

	printf("SET_SAMPLE_RATE %s\n", sample_rate);
	return EXIT_SUCCESS;
}

static void reset_state(struct simulator *state)
{
	state->sample_length = DEFAULT_SAMPLE_LEN;
	state->porta = DEFAULT_PORT_A_MASK;
	state->portb = DEFAULT_PORT_B_MASK;
	state->capturing = false;
	state->usb_data_counter = 0;
	state->data_points_sampled = 0;
	state->porta_output = false;
	state->portb_output = false;
	/* don't set counters because those have to be done before checking
	   the packet counter */
	for (unsigned i = 0; i < MAX_PACKET_LEN; i++){
		state->buf[i] = 0;
	}
}

static int set_sample_length(int fd, struct simulator *state)
{
	uint8_t arg;
	uint64_t sample_len = 0;
	unsigned i;

	for (i = 1; i <= 8; i++) {
		if (read_byte(fd, &arg) != sizeof(arg)) {
			return EXIT_FAILURE;
		}

		sample_len |= ((uint64_t) arg) << (8 * (8 - i));
	}

	if (sample_len < MIN_SAMPLE_LEN || sample_len > MAX_SAMPLE_LEN) {
		/* 0 means continuous sampling */
		if (sample_len != 0) {
			fprintf(stderr, "Out of bounds sample length: %lu\n",
					(long unsigned) sample_len);
			return EXIT_FAILURE;
		}
	}

	state->sample_length = sample_len;

	printf("SET_SMAPLE_LEN %lu\n", (long unsigned) sample_len);
	return EXIT_SUCCESS;
}

static int select_channels(int fd, struct simulator *state)
{
	uint8_t args[2];

	if (read_byte(fd, &args[0]) != sizeof(args[0])) {
		return EXIT_FAILURE;
	}
	if (read_byte(fd, &args[1]) != sizeof(args[1])) {
		return EXIT_FAILURE;
	}

	state->porta = args[0];
	state->portb = args[1];
	printf("SEL_CHANNELS: Port A: 0x%x, Port B: 0x%x\n", args[0], args[1]);
	return EXIT_SUCCESS;
}

static int usb_setting_return(int fd, int status)
{
	char func_ret;

	if (status == EXIT_SUCCESS) {
		func_ret = USB_SETTING_SUCCESS;
	} else {
		func_ret = USB_SETTING_ERROR;
	}

	return write_byte(fd, func_ret);
}

static int set_port_a(int fd)
{
	uint8_t arg;
	if (read_byte(fd, &arg) != sizeof(arg)) {
		return EXIT_FAILURE;
	}

	printf("Output A is: %x\n", arg);
	return EXIT_SUCCESS;
}

static int set_port_b(int fd)
{
	uint8_t arg;
	if (read_byte(fd, &arg) != sizeof(arg)) {
		return EXIT_FAILURE;
	}
	printf("Output B is: %x\n", arg);
	return EXIT_SUCCESS;
}

static int fill_in_port(int fd, struct simulator *state, uint8_t opcode)
{
	size_t i;
	uint8_t arg;
	if (read_byte(fd, &arg) != sizeof(arg)) {
		return EXIT_FAILURE;
	}
	state->buf[PACKET_OPCODE] = opcode;
	state->buf[PACKET_COUNTER] = 0; /* leave the counter blank until we send */

	for (i = PACKET_DATA_START; i < MAX_PACKET_LEN; i++) {
		state->buf[i] = arg;
	}
	return EXIT_SUCCESS;
}

static int fill_in_porta(int fd, struct simulator *state)
{
	return fill_in_port(fd, state, PORTA_SAMPLES);
}

static int fill_in_portb(int fd, struct simulator *state)
{
	return fill_in_port(fd, state, PORTB_SAMPLES);
}

static int cmd_echo(int fd)
{
	uint8_t data[MAX_PACKET_LEN - PACKET_DATA_START - 1];
	uint8_t num_bytes;
	ssize_t n_read, n_written;

	printf("CMD_ECHO\n");

	n_read = read(fd, &num_bytes, 1);
	if (n_read != 1) {
		fprintf(stderr, "Error reading echo count: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}
	if (num_bytes > (MAX_PACKET_LEN - PACKET_DATA_START - 1)) {
		fprintf(stderr, "Invalid num_bytes = %u\n", (unsigned) num_bytes);
		return EXIT_FAILURE;
	}

	n_read = read(fd, &data, num_bytes);
	if (n_read == -1) {
		fprintf(stderr, "Error reading echo data: %s\n", strerror(errno));
		return EXIT_FAILURE;
	} else if (n_read != num_bytes) {
		fprintf(stderr, "Partial read of echo data: %li/%u\n",
				(long) n_read, num_bytes);
		return EXIT_FAILURE;
	}

	/* okay so we got the data we need. Now we send the command back */
	n_written = write(fd, data, num_bytes);
	if (n_written == -1) {
		fprintf(stderr, "Error writing echo response: %s\n", strerror(errno));
		return EXIT_FAILURE;
	} else if (n_written != num_bytes) {
		fprintf(stderr, "Partial write of echo response: %li/%u\n",
				(long) n_written, num_bytes);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int handle_command(int fd, uint8_t cmd[2], struct simulator *state)
{
	int ret = EXIT_SUCCESS;

	/* first check the counter */
	if (cmd[PACKET_OPCODE] == CMD_RESET) {
		state->send_counter = 0;
		state->recv_counter = 0;
	}
	uint8_t expected = __get_counter(&state->recv_counter);
	if (cmd[PACKET_COUNTER] != expected) {
		fprintf(stderr, "Rx counter missmatch: expected %i got %i\n",
				expected, state->recv_counter);
		fprintf(stderr, "NAKing 0x%x\n", cmd[PACKET_OPCODE]);
		return usb_setting_return(fd, EXIT_FAILURE);
	}

	switch (cmd[PACKET_OPCODE]) {
		case CMD_SET_SAMPLE_RATE:
			ret = set_sample_rate(fd);
			break;
		case CMD_SET_CAPTURE_LENGTH:
			ret = set_sample_length(fd, state);
			break;
		case CMD_SEL_CHANNELS:
			ret = select_channels(fd, state);
			if (state->porta == 0)
			{
				state->porta_output = true;
			}
			if (state->portb == 0)
			{
				state->portb_output = true;
			}
			break;
		case CMD_BEGIN_ACQ:
			printf("BEGIN_ACQ\n");
			state->capturing = true;
			state->usb_data_counter = 0;
			state->data_points_sampled = 0;
			break;
		case CMD_END_ACQ:
			printf("END_ACQ\n");
			state->capturing = false;
			break;
		case CMD_LED_SET_RED:
			printf("LED_SET_RED\n");
			break;
		case CMD_LED_SET_GREEN:
			printf("LED_SET_GREEN\n");
			break;
		case CMD_LED_SET_BLUE:
			printf("LED_SET_BLUE\n");
			break;
		case CMD_LED_SET_OFF:
			printf("LED_SET_OFF\n");
			break;
		case CMD_PING:
			printf("PING\n");
			break;
		case CMD_RESET:
			printf("RESET\n");
			reset_state(state);
			break;
		case PORTA_SAMPLES:
			/* fallthrough */
		case PORTB_SAMPLES:
			printf("SAMPLE PACKET\n");
			fprintf(stderr, "Sending samples to the firmware doesn't make sense. Ignoring.\n");
			break;
		case CMD_CHOOSE_PINS_A:
			if ((state->porta_output) && (state->portb == DEFAULT_PORT_B_MASK) && (!state->portb_output))
			{
				state->porta_output = true;
				ret = fill_in_porta(fd, state);
			}
			else
			{
				ret = set_port_a(fd);
			}
			break;
		case CMD_CHOOSE_PINS_B:
			if ((!state->porta_output) && (state->porta == DEFAULT_PORT_A_MASK) && (state->portb_output))
			{
				state->portb_output = true;
				ret = fill_in_portb(fd, state);
			}
			else
			{
				ret = set_port_b(fd);
			}
			break;
		case CMD_ECHO:
			/* don't do a usb_setting_return */
			return cmd_echo(fd);
		default:
			fprintf(stderr, "Unrecognised command 0x%x\n", (int) cmd[PACKET_OPCODE]);
			return EXIT_FAILURE;
	}

	return usb_setting_return(fd, ret);
}

static int check_pending_command(int fd, struct simulator *state)
{
	uint8_t cmd[2];
	ssize_t n_read;

	errno = 0;
	n_read = read(fd, &cmd, 2);
	if (n_read == 2) {
		return handle_command(fd, cmd, state);
	} else if (n_read == 0) {
		/* EOF. We can try again after a sleep */
		return EXIT_SUCCESS;
	} else if (n_read == -1 && (errno == EIO || errno == EWOULDBLOCK)) {
		/* other side disconnected. We can try again after a sleep */
		return EXIT_SUCCESS;
	} else {
		fprintf(stderr, "Error reading from serial: %s\n",
			strerror(errno));
		return EXIT_FAILURE;
	}
}

static void fill_in_test_pattern_porta(uint8_t buf[], size_t len)
{
	size_t i;

	buf[PACKET_OPCODE] = PORTA_SAMPLES;
	buf[PACKET_COUNTER] = 0; /* fill in later */

	/* port A samples count up from 0 */
	for (i = 2; i < len; i++) {
		buf[i] = (uint8_t) i;
	}
}

static void fill_in_test_pattern_portb(uint8_t buf[], size_t len)
{
	size_t i;

	buf[PACKET_OPCODE] = PORTB_SAMPLES;
	buf[PACKET_COUNTER] = 0; /* fill in later */

	/* port B samples count down */
	for (i = PACKET_DATA_START; i < len; i++) {
		buf[i] = (uint8_t) (len - i);
	}
}


static int simulate(int port_file)
{
	struct simulator state;
	uint8_t buf_a[MAX_PACKET_LEN];
	uint8_t buf_b[MAX_PACKET_LEN];
	unsigned loop_count = 0;
	ssize_t written;

	reset_state(&state);

	for (;;) {
		if(state.porta_output || state.portb_output)
		{
			if (state.porta_output)
			{
				for (unsigned i = PACKET_DATA_START; i < MAX_PACKET_LEN; i++){
					buf_b[i] = state.buf[i];
					buf_a[i] = 0;
				}
			}
			else if (state.portb_output)
			{
				for (unsigned i = PACKET_DATA_START; i < MAX_PACKET_LEN; i++){
					buf_a[i] = state.buf[i];
					buf_b[i] = 0;
				}
			}
		}
		else
		{
			fill_in_test_pattern_porta(buf_a, MAX_PACKET_LEN);
			fill_in_test_pattern_portb(buf_b, MAX_PACKET_LEN);
		}
		if (check_pending_command(port_file, &state) != EXIT_SUCCESS) {
			return EXIT_FAILURE;
		}

		if (state.capturing) {
			loop_count += 1;
			if (loop_count == NUM_SAMPLES) {
				loop_count = 0;

				if (state.porta) {
					buf_a[PACKET_COUNTER] = __get_counter(&state.send_counter);
					written = write(port_file, buf_a, MAX_PACKET_LEN);
					if (written != MAX_PACKET_LEN) {
						fprintf(stderr,
							"Failed to write packet buffer: %s\n",
							strerror(errno));
						return EXIT_FAILURE;
					}
				}

				if (state.portb) {
					buf_b[PACKET_COUNTER] = __get_counter(&state.send_counter);
					written = write(port_file, buf_b, MAX_PACKET_LEN);
					if (written != MAX_PACKET_LEN) {
						fprintf(stderr,
							"Failed to write packet buffer: %s\n",
							strerror(errno));
						return EXIT_FAILURE;
					}
				}
			}
		}

		/* slow down a bit so we don't use all the cpu busy looping */
		/* We can send up to 2 bytes per loop. USB can do 12Mb/s in
		   ideal conditions; assume once we run we complete the loop
		   instantaneously: we need a delay of 1/(12E6/16)s = 1/(12/16)us
		   so something like 1us should do*/
		usleep(1);
	}

	return EXIT_SUCCESS;
}

int main()
{
	int flags;
	int ret = EXIT_FAILURE;
	int pty_master;
	char *pty_name;
	struct termios term;

	/* create pseudo-terminal so we can pretend to be a serial port */
	pty_master = posix_openpt(O_RDWR | O_NOCTTY);
	if (pty_master == -1) {
		fprintf(stderr, "Could not open pseudo-terminal: %s\n",
			strerror(errno));
		return ret;
	}

	/* put master in non-blocking mode */
	flags = fcntl(pty_master, F_GETFL, 0);
	if (flags == -1) {
		fprintf(stderr, "fcntl(F_GETFL): %s\n", strerror(errno));
		return ret;
	}
	if (fcntl(pty_master, F_SETFL, flags | O_NONBLOCK) == -1) {
		fprintf(stderr, "fcntl(F_SETFL): %s\n", strerror(errno));
		return ret;
	}

	/* grant permissions on pty */
	if (grantpt(pty_master) == -1) {
		fprintf(stderr, "grantpt(): %s\n", strerror(errno));
		goto out;
	}

	/* put pseudo-terminal in raw mode so that control characters aren't
	 * messed with */
	if (tcgetattr(pty_master, &term) == -1) {
		fprintf(stderr, "tcgetattr(): %s\n", strerror(errno));
		goto out;
	}
	cfmakeraw(&term);
	if (tcsetattr(pty_master, TCSANOW, &term) == -1) {
		fprintf(stderr, "tcsetattr(): %s\n", strerror(errno));
		goto out;
	}

	/* allow pty slave to be opened */
	ret = unlockpt(pty_master);
	if (ret == -1) {
		fprintf(stderr, "unlockpt(): %s\n", strerror(errno));
		goto out;
	}

	/* get name of the pty slave */
	pty_name = ptsname(pty_master);
	if (!pty_name) {
		fprintf(stderr, "ptsname(): %s\n", strerror(errno));
		goto out;
	}
	printf("Slave pty is %s\n", pty_name);

	ret = simulate(pty_master);
out:
	close(pty_master);
	return ret;
}
