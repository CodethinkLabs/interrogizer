/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.h"
#include "usb_protocol.h"

enum command_args {
	PROG,
	SERIAL_PORT,
	CAPTURE_LENGTH,
	NUM_ARGS
};

int main(int argc, char *argv[])
{
	int ret = EXIT_FAILURE;;
	uint8_t cmd[9];
	long long int capture_length;

	if (argc != NUM_ARGS)
	{
		fprintf(stderr, "Usage:\n");
		fprintf(stderr, "%s <SERIAL_PORT> <CAPTURE_LENGTH>\n", argv[PROG]);
		/* Fail */
		return EXIT_FAILURE;
	}

	/* Open the port file */
	int port_file;
	port_file = open_tty(argv[SERIAL_PORT]);
	if (port_file == -1) {
		return EXIT_FAILURE;
	}

	errno = 0;
	capture_length = strtoll(argv[CAPTURE_LENGTH], NULL, 10);
	if (errno != 0) {
		fprintf(stderr, "Invalid number '%s': %s\n", argv[CAPTURE_LENGTH],
				strerror(errno));
		goto out;
	}
	if (capture_length < 0) {
		fprintf(stderr, "Negative capture length doesnt make sense\n");
		goto out;
	}

	if (set_capture_length(port_file, (uint64_t) capture_length) != EXIT_SUCCESS) {
		goto out;
	}

	assert_command(port_file, cmd, 9);
	ret = EXIT_SUCCESS;
out:
	/* Close the port file */
	close(port_file);
	return ret;
}
