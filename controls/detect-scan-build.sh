#!/bin/bash

HIGHEST_VER=10
LOWEST_VER=1

for version in $(seq "$HIGHEST_VER" -1 "$LOWEST_VER"); do
	BIN="scan-build-$version"
	if "$BIN" --help &>/dev/null; then
		# no error: advertise this version and exit
		echo "$BIN"
		exit 0
	fi
done

# None found. Warn to stdout
echo "WARNING: scan-build not found. Skipping static analysis." >&2
