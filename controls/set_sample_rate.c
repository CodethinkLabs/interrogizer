/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.h"
#include "usb_protocol.h"

enum command_args {
	PROG,
	SERIAL_PORT,
	SAMPLE_RATE,
	NUM_ARGS,
};

int main(int argc, char *argv[])
{
	int ret;
	uint8_t cmd[2];
	bool set_successfully = false;

	if (argc != NUM_ARGS)
	{
		fprintf(stderr, "Usage:\n");
		fprintf(stderr, "%s <SERIAL_PORT> <SAMPLE_RATE>\n", argv[PROG]);
		/* Fail */
		return EXIT_FAILURE;
	}

	/* Open the port file */
	int port_file;
	port_file = open_tty(argv[SERIAL_PORT]);
	if (port_file == -1) {
		return EXIT_FAILURE;
	}

	cmd[0] = CMD_SET_SAMPLE_RATE;

	/* Set the sample rate, if matches a supported value */
	if (strcmp(argv[SAMPLE_RATE], "200KHZ") == 0)
	{
		cmd[1] = SAMPLE_200KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "100KHZ") == 0)
	{
		cmd[1] = SAMPLE_100KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "50KHZ") == 0)
	{
		cmd[1] = SAMPLE_50KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "20KHZ") == 0)
	{
		cmd[1] = SAMPLE_20KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "10KHZ") == 0)
	{
		cmd[1] = SAMPLE_10KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "5KHZ") == 0)
	{
		cmd[1] = SAMPLE_5KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "2KHZ") == 0)
	{
		cmd[1] = SAMPLE_2KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "1KHZ") == 0)
	{
		cmd[1] = SAMPLE_1KHZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "500HZ") == 0)
	{
		cmd[1] = SAMPLE_500HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "200HZ") == 0)
	{
		cmd[1] = SAMPLE_200HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "100HZ") == 0)
	{
		cmd[1] = SAMPLE_100HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "50HZ") == 0)
	{
		cmd[1] = SAMPLE_50HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "20HZ") == 0)
	{
		cmd[1] = SAMPLE_20HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "10HZ") == 0)
	{
		cmd[1] = SAMPLE_10HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "5HZ") == 0)
	{
		cmd[1] = SAMPLE_5HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "2HZ") == 0)
	{
		cmd[1] = SAMPLE_2HZ;
		set_successfully = true;
	}
	if (strcmp(argv[SAMPLE_RATE], "1HZ") == 0)
	{
		cmd[1] = SAMPLE_1HZ;
		set_successfully = true;
	}

	if (set_successfully) {
		assert_command(port_file, cmd, 2);
		ret = EXIT_SUCCESS;
	} else {
		printf("Sample rate: '%s' not recognised\n", argv[SAMPLE_RATE]);
		ret = EXIT_FAILURE;
	}

	/* Close the port file */
	close(port_file);

	/* Complete */
	return ret;
}
