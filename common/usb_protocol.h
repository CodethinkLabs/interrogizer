/*
 * Copyright 2020 Codethink Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMMON_USB_PROTOCOL_H
#define COMMON_USB_PROTOCOL_H

/* USB command byte definitions */
/* Logic analyser commands */
#define CMD_SET_SAMPLE_RATE		0x01
#define CMD_SET_CAPTURE_LENGTH		0x02
#define CMD_SEL_CHANNELS		0x05
#define CMD_BEGIN_ACQ			0x06
#define CMD_END_ACQ			0x07
/* Comms bus commands */
/* DAC output commands */
/* LED control commands */
#define CMD_LED_SET_RED			0x08
#define CMD_LED_SET_GREEN		0x09
#define CMD_LED_SET_BLUE		0x0a
#define CMD_LED_SET_OFF			0x0b
/* Misc commands */
#define CMD_PING			0x0c
#define CMD_RESET			0x0d
#define CMD_ECHO			0x0e
/* Data packets */
#define PORTA_SAMPLES			0x10
#define PORTB_SAMPLES			0x11
/*Function generator commands*/
#define CMD_CHOOSE_PINS_A		0x20
#define CMD_CHOOSE_PINS_B		0x21

/* USB return values */
#define USB_SETTING_SUCCESS		0x41
#define USB_SETTING_ERROR		0x42

/* Supported sample rates */
#define SAMPLE_200KHZ	 	 2
#define SAMPLE_100KHZ	 	 3
#define SAMPLE_50KHZ 	 	 4
#define SAMPLE_20KHZ 	 	 5
#define SAMPLE_10KHZ 	 	 6
#define SAMPLE_5KHZ  	 	 7
#define SAMPLE_2KHZ  	 	 8
#define SAMPLE_1KHZ  	 	 9
#define SAMPLE_500HZ 	 	10
#define SAMPLE_200HZ 	 	11
#define SAMPLE_100HZ 	 	12
#define SAMPLE_50HZ  	 	13
#define SAMPLE_20HZ  	 	14
#define SAMPLE_10HZ  	 	15
#define SAMPLE_5HZ   	 	16
#define SAMPLE_2HZ   	 	17
#define SAMPLE_1HZ   	 	18

/* Supported sample lengths */
#define MIN_SAMPLE_LEN		100
#define MAX_SAMPLE_LEN		10000000000 /* 10G */

/* Packet structure for all commands and samples */
#define PACKET_OPCODE		0
#define PACKET_COUNTER		1
#define PACKET_DATA_START	2

/* Maximum size of data packet */
/* This needs to be even */
#define MAX_PACKET_LEN 64
#define NUM_SAMPLES (MAX_PACKET_LEN - PACKET_DATA_START)

#endif /* COMMON_USB_PROTOCOL_H */
