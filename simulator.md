# Simulator

There is a simulator in `controls/simulator.c`.

To build all of the controls run `make` in the controls directory. If you don't
have `scan-build` (often packaged with LLVM/clang) installed then use
`RELEASE=1 make`.

This creates binaries in `controls/bin/`.

To run the simulator run `./bin/simulator`. The output will look like this

```
$ ./bin/simulator

Slave pty is /dev/pts/4
```

This tells us that the virtual serial port for the simulator is at /dev/pts/4.

To run the controls, open another terminal window and try one, using /dev/pts/4
as an argument e.g.

```
$ ./bin/turn_led_red /dev/pts/4
```

This control doesn't print anything. You can see if it succeeded by its exit
code (in bash the exit code of the previously run command  is accessed with `$?`).
As for all UNIX programs, a exit code of 0 means success and non-zero means failure.

We can also see on the simulator window that it received the command:

```
$ ./bin/simulator
Slave pty is /dev/pts/4
PING
LED_SET_RED
```

Some controls (e.g. `set_sample_rate`) take more arguments. For more information
use the `--help` argument.
