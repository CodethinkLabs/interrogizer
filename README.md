The Interrogizer is a board designed to be used for debugging/protoyping 
purposes. It provides the user with a simple interface to measure and produce
signals on GPIOs. Interrogizer is mostly useful as a low bandwidth logic
analyser.

# Getting started

## Dependencies
    openocd
    gcc-arm-none-eabi (arm-none-eabi-gcc)
    arm-none-eabi-newlib

**Note:** The packaged version of OpenOCD for your distribution is likely far behind the current codebase and may not support the STLink V3 debugger. To avoid issues it's recommended that you build OpenOCD from source instead.

For guidance on that please see [this guide](https://mbd.kleier.net/integrating-st-link-v3.html).

## Setting up
    git clone https://gitlab.com/CodethinkLabs/interrogizer.git
    cd interrogizer
    git submodule update --init
    make -C firmware/libopencm3

## Building
You should now be ready to build the firmware for the Interrogizer using make,
you can do this by using `make` from the firmware directory, or using
`make -C firmware/` from the top level of the repo.

## Flashing

Connect the board's programming header (J3) to the debugger. You only need connect the RST, SWDIO, SWCLK and GND pins. These connect to their corresponding pins on CN6 of the STLink V3.

Ensure the board is connected to the host via USB as well as the debugger.

Use `make flash` to load the firmware onto the device.

## Control Programs and Automated Tests
There are some control programs in `controls/`. These can be built using `make`.
Static analysis using `scan-build` is performed automatically by the Makefile if
`scan-bulid` is found on your system. This is recommended.

The control programs provide a simpler and easier to debug way to talk with
an interrogizer device compared to the sigrok driver.

There are automated tests in this directory:
- `echo` uses the `echo` command to send data to and from the interrogizer device
  over USB. This is useful for debugging USB connections.
- `test_gen_read` exhaustively tries single-port GPIO output patterns and tests
  that these can be sampled by the other port. This is useful as a system-level
  test.

The control programs take a serial port over which to talk to the interrogizer
device as an argument. This will be `/dev/ttyACMn` where n is a positive
integer or 0. Look at `dmesg` when interrogizer is plugged in to determine which
serial port it is.

## Simulation
There is an interrogizer device simulator in the `controls` directory. When run
it will report the name of a virtual serial port to use with the control programs
or sigrok.

The simulator prints to the console saying what commands it has received. It will
produce dummy sample data when required. It should work with the automated
tests listed above.

## Debugging

To debug you will need `gdb` built for `arm-none-eabi` and `openocd` (see above).
If your distribution does not package this then `gdb` can be built using
`crosstool-ng` (**using crosstool.config in this repo**):

### Building arm-none-eabi-gdb
```
sudo apt-get install texinfo help2man build-essential flex autoconf unzip file gawk libtool libtool-bin bison libncurses-dev wget python
git clone https://github.com/crosstool-ng/crosstool-ng
cd crosstool-ng
./bootstrap
./configure --enable-local
make -j$(nproc)
cp /path/to/crosstool.conf .config
./ct-ng build
```
On fedora `libsdc++-static` is also needed as a build dependency.

The toolchain will be installed to `$HOME/x-tools/arm-unknown-eabi`. The
`crosstool.config` here is
`crosstool-ng/samples/arm-unknown-eabi/crosstool.config` with gdb enabled using
`./ct-ng menuconfig`.

### Debugging using gdb and openocd
First start `openocd` like this:

```
# openocd -f interface/stlink.cfg -f target/stm32f0x.cfg
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections
Info : clock speed 1000 kHz
Info : STLINK V3J1M1B1S1 (API v3) VID:PID 0483:374F
Info : Target voltage: 3.278243
Info : stm32f0x.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for stm32f0x.cpu on 3333
Info : Listening on port 3333 for gdb connections
```

As we can see here, `openocd` listens on `localhost:3333` for `gdb`. Tell `gdb`
about this using `target remote localhost:3333`.

Here's an example `gdb` session:

```
$ arm-unknown-eabi-gdb interrogizer.elf
GNU gdb (crosstool-NG 1.24.0.105_5659366) 9.1
Copyright (C) 2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "--host=x86_64-build_pc-linux-gnu --target=arm-unknown-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from interrogizer.elf...
(gdb) target remote localhost:3333
Remote debugging using localhost:3333
st_usbfs_poll (dev=0x20000144 <st_usbfs_dev>) at ../common/st_usbfs_core.c:271
271		if (istr & USB_ISTR_SUSP) {
(gdb) break main
Breakpoint 1 at 0x8000120: file usb-test.c, line 164.
(gdb) continue
Continuing
```

# Architecture
## Hardware:
The Interrogizer is based around an [STM32F072C8T6](https://www.st.com/en/microcontrollers-microprocessors/stm32f072c8.html). It has a USB B full
size input for the USB full speed connection to the device. The board has a
20 pin output header with GND, 5V, 3.3V and 16 I/O broken out.

## Firmware:
The firmware is based on [LibOpenCM3](https://github.com/libopencm3/libopencm3) revolves around the USB CDC connection to the host PC. Once device has been enumerated, the host driver software will send commands to determine what function the device fulfills.  

USB Commands are polled and each command will be mapped to a corresponding
function in the firmware.

## Software:
Sigrok driver - This is essentially a wrapper around the driver that sends
the commands to the device.

# Command structure:
Each packet has an op-code 8 bits wide and an 8-bit wide counter. Depending on
the command it may be necessary to append arguments after the op-code and counter.
The size of the data to be appended depends on the command in question.
Constants are in `common/usb_protocol.h`.

    Command list:

    Command                         Data size
    Set sample rate                 8 bit (SAMPLE_*HZ)
    Set capture length              64 bit (big endian unsigned capture length)
    Select channels                 16 bit (bit map for each active pin)
    Begin acquisition               0 bit
    End acquisition                 0 bit
    Set LED red                     0 bit
    Set LED green                   0 bit
    Set LED blue                    0 bit
    Set LED off                     0 bit
    Ping                            0 bit
    Reset                           0 bit
    Echo                            (1 byte length then up to 62 bytes data)
    Choose pins A                   8 bit port a bitmap
    Choose pins B                   8 bit port b bitmap

    Other opcodes
    PORTA_SAMPLES                   Samples taken on port A
    PORTB_SAMPLES                   Samples taken on port B
    USB_SETTING_SUCCESS             Positive acknowledgement for command
    USB_SETTING_ERROR               Negative acknowledgement for command

Commands sent to the device are answered with either `USB_SETTING_SUCCESS` or
`USB_SETTING_ERROR`. These packets do not use a counter.

Sample packets from the device to the host contain 1 byte set to either
`PORT*_SAMPLES`, then the counter byte, and finally 62 bytes of samples.

There is a separate counter for each transfer direction. The counters start at
0 and increment for each packet send or received, wrapping back to 0 when the
count overflows. Reset commands should have a counter of 0.

Functionality mostly works. The main work now is ironing out bugs

- First hardware revision: Done
- Device Firmware:
  - USB CDC - Done and tested.
  - Logic analyser - Done and tested
- Host software:
  - Sigrok Driver - Done

## Using sigrok-cli
Use https://gitlab.com/CodethinkLabs/interrogizer-libsigrok

To fetch 1000 samples using a device attached at `/dev/ttyACM1` at 100Hz:

```
sigrok-cli -d codethink-interrogizer:conn=/dev/ttyACM1 -c samplerate=100 --samples 1000
```
