# Hardware faults
The boards have a lot of manufacturing faults. These are documented here

## ecclescake
### Board with wires (serial #0)
This board has extra wires soldered to probe USB and to access pins defined in
`debug_pins.h`.

- C1 missing

### Unmodified board (serial #1)
- C11 tombstoned
- C15 tombstoned

## msim

### Board with wires (serial #2)
This board has extra wires soldered to probe USB and to access pins defined in
`debug_pins.h`.

- C1 missing

### Unmodified board (serial #3)
- C1 missing
- C8 tombstoned
- C4 tombstoned
- C5 tombstoned
- C11 missing
- C15 was tombstoned, questionably repaired

## adamrich

### Unmodified board (Serial #4)
- C5 tombstoned
- C11 tombstoned
- C14 tombstoned

### Unmodified board Serial #5
- C6 tombstoned
